'use strict';

module.exports = function(grunt){

  /**
   * Extremely basic Gruntfile.
   * Has a lot of room for improvement, but gets the job done for now.
   * - Milton Plotkin
   */

  /* Configure
  ============================ */

  var configs = {   
    
    css_combine_files : [
      'dist/_temp/styles.css'
    ],
    
    js_hint_files : [
      'src/core/**/*.js',
      'src/extensions/**/*.js',
      'src/components/**/*.js',
      'src/pages/**/*.js',
      '!**/libraries/**/*.js'
    ],

    watch_files : [
      'src/core/**',
      'src/components/**',
      'src/extensions/**',
      'src/required/**',
      'src/content/**',
      'src/pages/**'
    ]
  }

  // Custom tasks
  // ==========================
  
  // https://stackoverflow.com/questions/15647276/grunt-js-output-and-write-contents-of-folder-to-file/15649191
  grunt.registerMultiTask("dir2Json", "Creates a JSON file containing an array of all files in a directory.", function() {
    let paths = grunt.file.expand(this.data.options, this.data.paths),
    out = this.data.output,
    contents = {};

    paths.forEach((path) => {
      let filename = path.match(/\/([^/]*)$/)[1];
      let name = filename.substr(0, filename.lastIndexOf('.')) || filename;
      // Store the filename and the path to the file (the name will be used to get the template data in the main .js).
      contents[name] = path;
    });

    grunt.log.writeln(paths.length + " template file(s) found.");
    grunt.file.write(out, JSON.stringify(contents));
  });

  grunt.registerMultiTask('jsonsynt-build', 'Builds *.jsonsynt into *.json', function() {
    let paths = grunt.file.expand(this.data.options, this.data.paths);
    
    paths.forEach((filepath) => {

      // Export to the same filename, but with a different extension.
      var filepathJSONSYNT = filepath.replace(/[.]jsonsynt$/, ".json");

      let currentFileString = grunt.file.read(this.data.options.cwd + '/' + filepath);
      let finalStr = currentFileString;

      /**********************/
      /* <<<! COMMENTS !>>> */
      /**********************/
      // Find and remove any JSONSYNT comments.
      var regSyntComment = /(<<<!)([\s\S]*?)(!>>>)/g;
      
      // Find each match, and replace the matched string at that point.
      finalStr = finalStr.replace(regSyntComment, (match, $1, $2, $3) => {
        return "";
      });

      /*****************************************/
				/* <code>
				/* <pre>
				/* {{#code}}
				/* and <<<P ... P>>> tags
				/*****************************************/

        let defaultLang = "html";
				const tabHTML = '<span class="tab">&Tab;</span>';

				function formatCodeContent(contentStr) {

					let s = contentStr;

					s = s.replace(/(^\s*\n)|(\s*\n\s*$)/g, ''); // Remove leading and trailing tab & returns.

					// FIXME: DD specific implementation. Doesn't allow for easy customization. \/
					s = s.replace(/[\n]/g, '<br class="br" />'); // Replace newline characters with "<br/>". The class is to help them stand out from any manually entered <br> tags.
					s = s.replace(/[\t]/g, tabHTML);		// Replace tab characters with <span> elements.

					// Handlebars automatically interprets any literal '\n' strings in text and converts them to newline characters.
					// When we use \n in text, we always want it to be literally shown as '\n', so the following prevents this from happening.
					// (Also applies to \t and \r).
					s = s.replace(/\\n/g, "\\\\n"); 		// Escape unescaped '\n' strings in text with '\\n'.
					s = s.replace(/\\t/g, "\\\\t"); 		// Escape unescaped '\t' strings in text with '\\t'.
					s = s.replace(/\\r/g, "\\\\r"); 		// Escape unescaped '\r' strings in text with '\\r'.

					return s;
				}

				function formatCodeTags(regPre) {
					finalStr = finalStr.replace(regPre, (match, $1, $2, $3, $4) => {

						var _3 = $3;

						// Try to detect what language is being used (special treatment for "html").
						// FIXME: This functionality is only really used by IT06.
						if ($2 == "code" || $2 == "pre") {
							let lang = $1.match(/lang="(.*?)"/);
							if (lang) lang = lang[1];
							lang = lang || defaultLang; // If no language specified, assume it's using the default language (if any).

							// If the language is "html", escape all < and >.
							// However, do NOT escape << and >>
							// FIXME: Does not distinguish between <tag /> and <tag title="<tag>" />.
							if (lang == "html") { 

								_3 = _3.replace(/<</g, "!&LT;&LT;!");
								_3 = _3.replace(/>>/g, "!&GT;&GT;!");

								_3 = _3.replace(/</g, "&lt;");
								_3 = _3.replace(/>/g, "&gt;");

								// Don't replace instances of {{>
								//_3 = _3.replace(/([^{])(>)/g, (match, $1, $2) => {return $1 + "&gt;"});

                // Escape all { and }.
                _3 = _3.replace(/{/g, "&lcub;");
                _3 = _3.replace(/}/g, "&rcub;");

								// Un-escape !&LT;&LT!; and !&GT;&GT;!.
								_3 = _3.replace(/!&LT;&LT;!/g, "<");
								_3 = _3.replace(/!&GT;&GT;!/g, ">");

                // Special case: have <mark> </mark> tags be unescaped.
                // If the opening <mark> tag has nothing after it, assume it's a block.
                _3 = _3.replace(/&lt;\s*?mark(.*?)&gt;(\s*?\S?)/g, (match, $1, $2) => {
                  return "<mark " + $1 + ($2.trim().length ? "" : "data-is-block=\"true\"") + ">" + $2;
                });
                _3 = _3.replace(/&lt;\/\s*?mark.*?&gt;/g, "</mark>");
							}
						}
            
            // Remove any newlines directly after a <mark>.
            // FIXME: This isn't working. Handled with CSS now.
            /*_3 = _3.replace(/(&lt;\s*?mark.*?&gt;)([\n\r])/gm, (match, $1) => {
              return $1;
            });*/

						_3 = formatCodeContent(_3);

            // Remove {{#pre}} tags (Adapt template guides specific).
            if ($2 == "pre") {
              return "<code class='example'>" + _3 + "</code>";
            }

						return $1 + _3 + $4;
					});
				}

				// <code> and <pre> tags use preformatted style HTML. As such, their indents and return keys need to be preserved.
				// \2 matches capture group #2.
				// TODO: Increase flexibility.
				// Find and replace all tab and return characters inside the code / pre tags.
				//formatCodeTags(/(<(code|pre).*?>)([\s\S]*?)(<\/\2>)/g);
				formatCodeTags(/({{#(pre).*?}})([\s\S]*?)({{\/\2}})/g);

				var regPre1 = /(<<<P)([\s\S]*?)(P>>>)/g;

				// Find and replace all tab and return characters inside the <<<P ... P>>> tags.
				finalStr = finalStr.replace(regPre1, (match, $1, $2, $3) => {
					
					var _2 = $2;
					
					// Format HTML
					let lang = defaultLang;
					if (lang == "html") { 
						_2 = _2.replace(/</g, "&lt;");
						_2 = _2.replace(/>/g, "&gt;");
					}

					_2 = formatCodeContent(_2);

					return '<<<"' + _2 + '">>>';
				});
      
      /*************/
      /* <<<" ">>> */
      /*************/
      var regSynt = /(<<<")([\s\S]*?)(">>>)/g;
      
      // Find each match, and replace the matched string at that point.
      finalStr = finalStr.replace(regSynt, (match, $1, $2, $3) => {
        
        var m = match;
        
        // Remove returns and tabs. Also remove the opening <<<" and closing ">>> .
        m = m.replace(/[\t\r\n]|(<<<")|(">>>)/g, '');
        
        // Find and escape all non-escaped double quote (") marks.
        m = m.replace(/(\\)?(")/g, (match2, $1, $2) => {
          if ($1) return match2; // This is already an escaped quote. Do nothing.
          return "\\\"";
        });
        
        // Add normal "" around the string (so that it will be a valid JSON string).
        m = "\"" + m + "\"";
        
        return m;
      });

      // ===================================================
      // Write the modified *.jsonsynt file string to *.json
      // ===================================================
      grunt.file.write(this.data.options.cwd + '/' + filepathJSONSYNT, finalStr);

    });	
  });

  grunt.registerMultiTask("combinePageJson", "Combines all page json files in a directory.", function() {
    let paths = grunt.file.expand(this.data.options, this.data.paths),
    out = this.data.output,
    contents = {
      pages: {}
    };

    paths.forEach((path) => {

      let split = path.split('/');

      let name = split[split.length-2]; // The name of the directory is the name of the template.
      let type = split[split.length-3]; // This is the type of the page.

      contents.pages[name] = grunt.file.readJSON(this.data.options.cwd + '/' + path);
      contents.pages[name].type = type;

      contents.pages[name]._assetsPath = "content/pages/" + path;

      /*let filename = path.match(/\/([^/]*)$/)[1];
      let name = filename.substr(0, filename.lastIndexOf('.')) || filename;
      // Store the filename and the path to the file (the name will be used to get the template data in the main .js).
      contents[name] = path;*/
    });

    grunt.log.writeln(paths.length + " page file(s) found.");
    grunt.file.write(out, JSON.stringify(contents));
  });

  grunt.registerMultiTask("extensions2Json", "Temp. Create a JSON with information on the extensions.", function() {
    let paths = grunt.file.expand(this.data.options, this.data.paths),
    out = this.data.output,
    contents = [

    ];

    let temp = {};

    // FIXME: Super hardcoded.

    paths.forEach((path) => {

      let extensionName = path.split("/")[1];
      temp["extensions/" + extensionName + "/js/" + extensionName + ".js"] = true;

      /*let split = path.split('/');

      let name = split[split.length-2]; // The name of the directory is the name of the template.
      let type = split[split.length-3]; // This is the type of the page.

      contents.pages[name] = grunt.file.readJSON(this.data.options.cwd + '/' + path);
      contents.pages[name].type = type;*/

      /*let filename = path.match(/\/([^/]*)$/)[1];
      let name = filename.substr(0, filename.lastIndexOf('.')) || filename;
      // Store the filename and the path to the file (the name will be used to get the template data in the main .js).
      contents[name] = path;*/
    });

    Object.keys(temp).forEach(key => {
      grunt.log.writeln(key);
      contents.push(key);
    });

    grunt.log.writeln(paths.length + " extension(s) found.");
    grunt.file.write(out, JSON.stringify(contents));
  });

  /* Init
  ============================ */
  grunt.initConfig({

    less: {
      production: {
        files: [{
          "dist/_temp/styles.css" : "dist/_temp/styles.less"
        }]
      }
    },
    connect: {
      server: {
        options: {
          port: 8000,
          hostname: "*",
          keepalive: true,
          open: true,
          base: "dist"
        }
      }
    },
    jshint: {
      options: {
        ignores: ['src/**/libraries/**/*', 'src/**/lib/**/*'],
        force: true,
        esversion: 8,     
        asi: true // Ignore warnings about semicolons.
      },
      all: [
        'src/core/**/*.js',
        'src/extensions/**/*.js',
        'src/components/**/*.js',
        'src/pages/**/*.js'
      ]
    },

    uglify: {
        my_target: {
          files: {
            'dist/js/compiled.min.js' : 'src/js/compiled.js'
          }
        }
    },
    cssmin: {
      combine: {
        files: {
          'dist/css/styles.css' : configs.css_combine_files
        }
      }
    },
    watch: {
      options: {
        atBegin: true, // Run the build upon starting grunt watch.
        spawn: false // Speeds up clean?
      },
      src: {
        files: configs.watch_files,
        tasks: ['build-watch']
      }
    },

    clean: {
      build: ["dist"],
      temp: ["dist/_temp"]
    },

    copy: {
      libraries: {
        files: [
          {
            expand: true,
            flatten: true,
            src: ['src/**/libraries/**/*'],
            dest: 'dist/libraries/',
            filter: 'isFile'
          }
        ]
      },
      js: {
        files: [
          // Core
          {
            expand: true,
            flatten: false,
            cwd: 'src/core/',
            src: ['**/*.js', '!**/libraries/**/*'],
            dest: 'dist/core/'
          },
          // Components and extensions.
          {
            expand: true,
            flatten: false,
            cwd: 'src/',
            src: ['components/**/*.js', 'extensions/**/*.js', 'pages/**/*.js', '!**/libraries/**/*'],
            dest: 'dist/'
          }
        ]
      },
      // HTML + initial .js that's loaded on pageload.
      required: {
        files: [
          {
            expand: true,
            flatten: false,
            cwd: 'src/required/',
            src: ['**/*.*'],
            dest: 'dist/'
          }
        ]
      },
      assets: {
        files: [
          {
            expand: true,
            flatten: true,
            cwd: 'src/',
            src: [
              '**/assets/**/*.*',
              '!**/assets/**/Arimo**', // FIXME: Temp.
              '!**/assets/**/Kalam**'  // FIXME: Temp.
            ],
            dest: 'dist/assets/'
          },
          {
            expand: true,
            flatten: true,
            cwd: 'src/',
            src: [
              '**/assets/**/Arimo**'
            ],
            dest: 'dist/assets/fonts/Arimo/'
          },
          {
            expand: true,
            flatten: true,
            cwd: 'src/',
            src: [
              '**/assets/**/Kalam**'
            ],
            dest: 'dist/assets/fonts/Kalam/'
          }
        ]
      },
      fonts: {
        files: [
          {
            expand: true,
            flatten: true,
            cwd: 'src/',
            src: ['**/fonts/**/*.*'],
            dest: 'dist/css/fonts/'
          }
        ]
      },
      templates: {
        files: [
          {
            expand: true,
            flatten: true,
            cwd: 'src/',
            src: ['**/templates/**/*.hbs'],
            dest: 'dist/templates/'
          }
        ]
      },
      content: {
        files: [
          {
            expand: true,
            flatten: false,
            cwd: 'src/content/',
            src: ['**/*.*', '!**/page.json', '!**/page.jsonsynt'], // Copy everything except for the page.json, which has already been combined.
            dest: 'dist/content/'
          }
        ]
      },
      adaptPreviewBuild: {
        files: [
          {
            expand: true,
            flatten: false,
            cwd: 'src/adapt-preview/',
            src: ['**/*.*'], // Copy everything from the adapt preview build.
            dest: 'dist/adapt-preview/'
          },
          {
            expand: true,
            flatten: false,
            cwd: 'src/adapt-hybrid-preview/',
            src: ['**/*.*'], // Copy everything from the adapt preview build.
            dest: 'dist/adapt-hybrid-preview/'
          }
        ]
      }
    },

    concat: {
      less: {
        options: {
          separator: '\n',
        },
        src: [
          'src/**/less/**/*.less',
          '!src/adapt-preview/**/*.less',
          '!src/adapt-hybrid-preview/**/*.less'
        ], // Combine all less except that in adapt-preview.
        dest: 'dist/_temp/styles.less',
      }
    },

    dir2Json: {
      templates: {
        options: {
          cwd: 'dist/'
        },
        paths: ["templates/*.hbs"],
        output: "dist/templateData.json"
      }
    },
    extensions2Json: {
      extensions: {
        options: {
          cwd: 'src/'
        },
        paths: ["extensions/**/*.js"],
        output: "dist/extensionData.json"
      }
    },
    'jsonsynt-build': {
      pages: {
        options: {
          cwd: 'src/content/pages/'
        },
        paths: ["**/*.jsonsynt"]
      }
    },
    combinePageJson: {
      pages: {
        options: {
          cwd: 'src/content/pages/'
        },
        paths: ["**/page.json"],
        output: "dist/pageData.json"
      }
    }
  });

  // Add plugins
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-includes');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-connect');

  // Register tasks
  var tasks = [
    //'clean:build',    // Clear the build folder entirely.
    
    'jsonsynt-build:pages',    // Convert all .jsonsynt files to a .json file.

    'copy:libraries',   // Copy all library files from across the src to a single location.
    'copy:required',    // Copy HTML + js

    'concat:less',      // Combine all LESSCSS into one file in the temp folder.
    'less',             // Compile the above LESSCSS.
    'cssmin',

    //'jshint:all',

    // \/ Could be improved.
    'copy:js',          // Copy the non-library js from the source.
    'copy:assets',      
    'copy:fonts',      
    'copy:templates',           
    'dir2Json:templates',
    'extensions2Json:extensions',
    'combinePageJson:pages',  // Combine all page data into one large file (for much faster loading).
    'copy:content', 
    'copy:adaptPreviewBuild'
    

    /*
    
    //'concat:dist1',
    'uglify',
    'jshint',*/
    //'clean:temp'       // Remove temp files
  ];


  grunt.registerTask('build', tasks);
  grunt.registerTask('dev', "watch");
  grunt.registerTask('default', tasks);

  grunt.registerTask('server', "connect:server");

  // DO NOT CALL THE TASK CLEAN
  // https://stackoverflow.com/questions/42532151/grunt-clean-task-hangs
  grunt.registerTask('clean', ['clean:build']); // The clean tasks is a EXTREMELY slow for some reason. Excluded from grunt watch for this reason. Run this instead.
  grunt.registerTask('build-watch', tasks);

  grunt.event.on('watch', function(action, filepath) {
    grunt.log.writeln(filepath + ' has ' + action);
  });

};


/*

CMD commands:
grunt watch

*/