define([
    "dd",
    "core/js/views/componentView"
  ], function(DD, ComponentView) {

    /**
     * Shows preview layouts for the provided page.
     */

    class CompDescriptionView extends ComponentView {

    }

    CompDescriptionView.template = "description";
    return CompDescriptionView;
});