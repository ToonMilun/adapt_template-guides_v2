define([
    "dd",
	"core/js/views/componentView",
	"core/js/models/thumbsModel",
    "core/js/views/thumbsView"
  ], function(DD, ComponentView, ThumbsModel, ThumbsView) {

	/**
	 * Shows preview layouts for the provided guidePage.
	 */

	class ModularitySingleton extends Backbone.Model {
	
		initialize() {
			
			DD.pages.guides.each((pageModel) => {
				try {
					this.setUpGuidePageModularityData(pageModel);
				}
				catch (err) {
					throw err;
				}
			});
		}

		setUpGuidePageModularityData(pageModel) {
			
			let pageCollection = DD.pages.guides;

			// Get the modularity data from the guide page's page.json.
			let mod;
			try {
				mod = pageModel.get("components").modularity._parents;
			} 
			catch (err) {
				DD.log.warn("GuidePage \"" + pageModel.get("name") + "\" has no modularity property defined.");
				return;
			}
			
			// Find and link every valud parent guide page of this guide page.
			let parentPages = [];

			// _includeTags
			// -----------------------------------
			//console.log(pageModel.get("tags"));
			_.each(mod._includeTags || [], (tagName) => {

				// If "all" is specified, then this page can be a child of ALL pages (except itself).
				if (tagName == "all") {
					pageCollection.each((p) => {
						if (p !== pageModel) {
							parentPages.push(p);
						}
					});
					return;
				}

				pageCollection.each((p) => {
					if (_.includes(p.get("tags"), tagName)) {
						parentPages.push(p);
					}
				});
			});
			parentPages = _.uniq(parentPages);
			
			// _excludeTags
			// -----------------------------------
			parentPages = _.filter(parentPages, (p) => {
				return !_.find(p.get("tags"), (tagName) => {
					return _.includes(mod._excludeTags, tagName);
				});
			});
			
			// _includePages
			// -----------------------------------
			_.each(mod._includePages, (pageName) => {

				let parentPage = pageCollection.find((p) => {
					return p.get("_id").toLowerCase() == pageName.toLowerCase();
				});
				
				if (!parentPage) {
					DD.log.error("Could not find module parent page with _id: " + pageName + "(" + pageModel.get("name") + ").");
					return;
				}

				// Store the parent page for later.
				parentPages.push(parentPage);
			});
			parentPages = _.uniq(parentPages);

			// _excludePages
			// -----------------------------------
			parentPages = _.filter(parentPages, (p) => {
				return !_.includes(mod._excludePages, p.get("_id"));
			});

			// Assign to the pageModel its parents, and to its parents the pageModel.
			pageModel.set("_modularityParents", _.uniq(parentPages));
			_.each(_.uniq(parentPages), (p) => {
				let children = p.get("_modularityChildren") || [];
				children.push(pageModel);

				p.set("_modularityChildren", children);
			});
		}
	} 
	new ModularitySingleton();

	class ModularityView extends ComponentView {

		init(){
			super.init();
		}

		postRender(){

			let $children = this.$(".modularity__children-items");
			let $parents = this.$(".modularity__parents-items");

			let pageModel = this.model.get("page");

			if (pageModel.get("_modularityChildren")?.length) {
				$children.html(new ThumbsView({model: new ThumbsModel(
					{_pages: new Backbone.Collection(pageModel.get("_modularityChildren") || [])}
				)}).$el);
			}

			if (pageModel.get("_modularityParents")?.length) {
				$parents.html(new ThumbsView({model: new ThumbsModel(
					{_pages: new Backbone.Collection(pageModel.get("_modularityParents") || [])}
				)}).$el);
			}

			/*

			// No children
			if (!children.length) {
				this.$(".modularity__children-header .modularity__header-body").text("This content can't have any children nested in it.");
				this.$(".modularity__children-header").addClass("is-red");
			}
			// Some children
			else {
				let cView = DD.pages.guides.renderThumbsView();
				cView.filterByPages(children);
				this.$(".modularity__children-items").html(cView.$el);
			}

			// No parents
			if (!parents.length) {
				this.$(".modularity__parents-header .modularity__header-body").text("This content can't be nested inside of any others.");
				this.$(".modularity__parents-header").addClass("is-red");
			}
			// Some parents
			else {
				let pView = DD.pages.guides.renderThumbsView();
				pView.filterByPages(parents);
				this.$(".modularity__parents-items").html(pView.$el);
			}*/
		}
	}

	ModularityView.template = "modularity";
	return ModularityView;
});
