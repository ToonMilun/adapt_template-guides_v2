define([
    "dd",
    "core/js/views/componentView",
    /*"./jsonBuilderGroupView",
    "./jsonBuilderHelperGroupView",*/
    "text!content/jsonTemplateProperties.json"
  ], function(DD, CompView, /*JsonBuilderGroupView, JsonBuilderHelperGroupView,*/ GlobalProperties) {

    /**
     * Shows information for the developer + JSON builder app.
     */
    const globalProperties = JSON.parse(GlobalProperties);

    class CompDeveloperView extends CompView {

        init() {
            super.init();
            this.setUpModelData();
        }
        
        setUpModelData() {

            // Set up descriptions & settings for each kind of property that will appear in the JSON templates.
            let json = this.model.get("json");

            let componentProperties = _.merge(
                {},
                globalProperties.global,
                globalProperties.component,
                json.properties
            );
            let extraProperties = _.merge(
                {},
                globalProperties.global,
                globalProperties.extra,
                json.properties
            );


            let blockProperties = _.merge(
                {},
                globalProperties.global,
                globalProperties.block,
                json.properties
            );
            let htmlProperties = _.merge(
                {},
                globalProperties.global,
                globalProperties.html,
                json.properties
            );
            let helperProperties = _.merge(
                {},
                globalProperties.helper,
                json.properties
            );

            this.model.set("_properties", {
                _componentProps: componentProperties,
                _extraProps: extraProperties,
                _blockProps: blockProperties,
                _htmlProps: htmlProperties,
                _helperProps: helperProperties
            });
        }

        postRender() {

            let $widget = this.$(".developer__widget");
            let json = this.model.get("json");
            let properties = this.model.get("_properties");

            // Render the normal JSONBuilder templates
            // ---------------------------------------
            _.each(json.blocks, (blockData) => {

                let props = _.merge({}, properties._blockProps, blockData.properties || {});
                let type = "block";

                $widget.append(new DD.JSONBuilder({
                    _desc:          blockData._desc,
                    _root:          blockData._items,
                    _properties:    props,
                    _type:          type,
                    _assetsPath:    this.model.get("assetsURL")
                }).$el);
            });
            
            _.each(json.components, (componentData) => {

                let props = _.merge({}, properties._componentProps, componentData.properties || {});
                let type = "component";

                $widget.append(new DD.JSONBuilder({
                    _desc:          componentData._desc,
                    _root:          componentData._items,
                    _properties:    props,
                    _type:          type,
                    _assetsPath:    this.model.get("assetsURL")
                }).$el);
            });

            // Render the JSONBuilder helpers
            // ---------------------------------------
            
            _.each(json.helpers, (helperData) => {

                let props = _.merge({}, properties._helperProps, helperData.properties || {});
                let type = "helper";

                $widget.append(new DD.JSONBuilder({
                    _desc:          helperData._desc,
                    _example:          helperData._example,
                    _root:          helperData._items,
                    _properties:    props,
                    _myProperties:  helperData.properties,
                    _type:          type
                }).$el);
            });

            _.each(json.extra, (extraData) => {

                let props = _.merge({}, properties._extraProps, extraData.properties || {});
                let type = "extra";
                
                $widget.append(new DD.JSONBuilder({
                    _desc:          extraData._desc,
                    _root:          extraData._items,
                    _noRoot:        true,
                    _properties:    props,
                    _type:          type
                }).$el);
            });

            // FIXME: Using the Helper view for the HTML view.
            _.each(json.html, (htmlData) => {

                let props = _.merge({}, properties._helperProps, htmlData.properties || {});
                let type = "html";

                $widget.append(new DD.JSONBuilder({
                    _desc:          htmlData._desc,
                    _example:       htmlData._example,
                    _root:          {},
                    _properties:    {},
                    _myProperties:  {},
                    _type:          type
                }).$el);
            });

            _.each(json.other, (otherData) => {

                let props = _.merge({}, properties._helperProps, otherData.properties || {});
                let type = "other";

                $widget.append(new DD.JSONBuilder({
                    _title:         otherData._title,
                    _desc:          otherData._desc,
                    _example:       otherData._example,
                    _root:          {},
                    _properties:    {},
                    _myProperties:  {},
                    _type:          type
                }).$el);
            });
        }
    }

    CompDeveloperView.template = "developer";
    return CompDeveloperView;
});