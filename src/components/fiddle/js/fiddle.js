define([
    "dd",
    "core/js/views/componentView"
  ], function(DD, ComponentView) {

    const MIXINS = `
    .table-theme-subheader() {
        background: #517c98;
        color: #ffffff;
        text-align: center
    }
    .table-theme-label() {
        background: #becdd8;
        color: #252525
    }
    `;
    
    /**
     * Renders a series of items which provide a toggleable Word / HTML window, alongside an output window.
     * 
     * Uses the minified CSS from Hybrid. Be sure to update it when appropriate.
     */
    class CompWordView extends ComponentView {
        
        events() {
            return {
                "click .js-fiddle-word-btn"     : "onWordBtnClick",
                "click .js-fiddle-html-btn"     : "onHtmlBtnClick",
                "click .js-fiddle-output-btn"   : "onOutputBtnClick"
            }
        }

        onWordBtnClick() {
            this.$(".js-fiddle-btn").removeClass("is-active");
            this.$(".js-fiddle-word-btn").addClass("is-active");
            this.$(".js-fiddle-slider").css({"left": "0"});
        }

        onHtmlBtnClick() {
            this.$(".js-fiddle-btn").removeClass("is-active");
            this.$(".js-fiddle-html-btn").addClass("is-active");
            this.$(".js-fiddle-slider").css({"left": "-100%"});
        }

        onOutputBtnClick() {
            this.$(".js-fiddle-btn").removeClass("is-active");
            this.$(".js-fiddle-output-btn").addClass("is-active");
            this.$(".js-fiddle-slider").css({"left": "-200%"});
        }
        
        postRender() {

            let page = this.model.get("page");
            let json = page.get("components").fiddle;

            // Set content of iframes.
            this.model.get("_items").forEach((item, idx) => {

                const $item = this.$(`.js-fiddle-item[data-index=${idx}]`);

                const $iframe = $item.find(`.js-fiddle-iframe`);
                $iframe.on("load", () => {
                    $iframe
                        .contents()
                        .find('html')
                        .css({
                            "padding": "2rem", 
                            "overflow": "auto"
                        })
                        .find("body")
                        .html(item._output.body || $item.find(".js-fiddle-html").text()); // Use the HTML provided if no output given.

                    // Add the Hybrid HTML and styles.
                    $iframe
                        .contents()
                        .find("head")
                        .html(`
<link rel="stylesheet" href="assets/normalize.css">
<link rel="stylesheet" href="assets/adapt.css">
<style>
    table {
        line-height: 1.5;
    }
</style>
<style type="text/less" rel="stylesheet/less">
    ${MIXINS}
    ${$item.find(".js-fiddle-less").text()}
</style>`);
                    
                    // Need to add the less script like this.
                    let doc = $iframe[0].contentWindow.document;
                    let obj = doc.createElement("script");
                    obj.type = "text/javascript";
                    obj.src = "libraries/less.js";
                    doc.body.appendChild(obj);

                });

                // Add copy btn.
                $item.find(".js-fiddle-word").append(new DD.CopyBtn($item.find(".js-fiddle-word-inner")).$el);
            });
        }
    }

    CompWordView.template = "fiddle";
    return CompWordView;
});