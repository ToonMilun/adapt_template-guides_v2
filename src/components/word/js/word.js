define([
    "dd",
    "core/js/views/componentView",
    "./wordBuilderView"
  ], function(DD, ComponentView, WordBuilderView) {

    /* Handlebars helpers */
    /* ---------------------------------------------------------- */
    let idImgNum = 0; // Used by "id-img" helper to create random img numbers. Loops once it hits 50.
    let idAudioNum = 0;
    let idVideoNum = 0;

    let MEDIA_OPEN = "";
    let MEDIA_CLOSE = "";

    var helpers = {

        "w-yellow": function() {
            let context = arguments[arguments.length-1];
            let html = '<span class="w__yellow">[' + context.fn(this) + ']</span>';
            return new Handlebars.SafeString(html);
        },

        "w-arrow": function() {
            let context = arguments[arguments.length-1];
            let html = '<span class="w__arrow">&lt;--</span>';
            return new Handlebars.SafeString(html);
        },

        "w-green": function() {
            let context = arguments[arguments.length-1];
            let html = '<div class="w__green">' + context.fn(this) + '</div>';
            return new Handlebars.SafeString(html);
        },

        "w-instruction": function() {
            let context = arguments[arguments.length-1];
            let html = '<span class="w__instruction">' + context.fn(this) + '</span>';
            return new Handlebars.SafeString(html);
        },

        "w-debug": function() {
            let context = arguments[arguments.length-1];
            let html = '<span class="w__debug not-copy"' + (context.hash.fade ? 'style="opacity: 0.3;"' : '') + '>' + context.fn(this) + '</span>';
            return new Handlebars.SafeString(html);
        },

        "w-placeholder": function() {
            return new Handlebars.compile('{{#w-debug}}XXXXX{{/w-debug}}')();
        },

        // Common ID text
        // ----------------------------------------------------------------

        "id-img": function() {
            //return new Handlebars.compile('{{#w-debug}}BU01_T1_P00X{{/w-debug}}')();
            if (idImgNum > 50) idImgNum = 0;
            idImgNum++;
            return new Handlebars.compile('<strong>' + MEDIA_OPEN + 'PN: {{#w-debug}}BU01_T1_P' + String(idImgNum).padStart(3, '0') + '{{/w-debug}}' + MEDIA_CLOSE + '</strong>')();
        },

        "id-audio": function() {
            //return new Handlebars.compile('{{#w-debug}}BU01_T1_P00X{{/w-debug}}')();
            if (idAudioNum > 50) idAudioNum = 0;
            idAudioNum++;
            return new Handlebars.compile('<strong>' + MEDIA_OPEN + 'AN: {{#w-debug}}BU01_T1_A' + String(idAudioNum).padStart(3, '0') + '{{/w-debug}}' + MEDIA_CLOSE + '</strong>')();
        },

        "id-video": function() {
            //return new Handlebars.compile('{{#w-debug}}BU01_T1_P00X{{/w-debug}}')();
            if (idVideoNum > 50) idVideoNum = 0;
            idVideoNum++;
            return new Handlebars.compile('<strong>' + MEDIA_OPEN + 'VN: {{#w-debug}}BU01_T1_V' + String(idVideoNum).padStart(3, '0') + '{{/w-debug}}' + MEDIA_CLOSE + '</strong>')();
        },

        // {{#id-item-header}}1{{/id-item-header}} ----> [R1][BU01_T1_P00X + any other notes about the item/graphic]
        "id-item-header": function() {
            let context = arguments[arguments.length-1];
            return new Handlebars.compile('R' + context.fn(this) + '][{{#id-img}}{{/id-img}}{{#w-debug}} + optional notes about the item/graphic{{/w-debug}}')();
        },

        "id-standup-text": function() {

            let context = arguments[arguments.length-1];
            let textonly = Boolean(context.hash.textonly) || Boolean(context.hash.basic);

            return new Handlebars.compile('StandupTextStandupTextStandupTextStandupTextStandupTextStandupTextStandupTextStandupTextStandupTextStandupTextStandupTextStandupTextStandupTextStandupTextStandupTextStandupTextStandupTextStandupTextStandupTextStandupTextStandupTextStandupTextStandupTextStandupTextStandupTextStandupTextStandupTextStandupText' + (!textonly ? '<br/>{{#w-debug}}{{/w-debug}}{{#w-debug fade=true}}<em>(OPTIONAL) Add additional extras/graphics/tables/etc. here.</em>{{/w-debug}}' : ''))();
        },

        "id-reveal-text": function() {

            let context = arguments[arguments.length-1];
            let textonly = Boolean(context.hash.textonly) || Boolean(context.hash.basic);

            return new Handlebars.compile('RevealTextRevealTextRevealTextRevealTextRevealTextRevealTextRevealTextRevealTextRevealTextRevealTextRevealTextRevealTextRevealTextRevealTextRevealTextRevealTextRevealTextReveal' + (!textonly ? '<br/>{{#w-debug}}{{/w-debug}}{{#w-debug fade=true}}<em>(OPTIONAL) Add additional extras/graphics/tables/etc. here.</em>{{/w-debug}}' : ''))();
        },

        "id-feedback-text": function() {

            let context = arguments[arguments.length-1];
            let textonly = Boolean(context.hash.textonly) || Boolean(context.hash.basic);

            return new Handlebars.compile('FeedbackTextFeedbackTextFeedbackTextFeedbackTextFeedbackTextFeedbackTextFeedbackTextFeedbackTextFeedbackTextFeedbackText')();
        },

        "id-instruction": function() {
            return new Handlebars.SafeString('InstructionTextInstructionTextInstructionText');
        }
    };

    // Register the helpers to the Handlebars object.
    for (var name in helpers) {
        if (helpers.hasOwnProperty(name)) {
             Handlebars.registerHelper(name, helpers[name]);
        }
    }
    /* ---------------------------------------------------------- */

    /**
     * Renders an element formated with the MS Word template for the currentl viewed template.
     * This can then be copy and pasted by the ID-ers to develop their ID docs.
     */
    class CompWordView extends ComponentView {
        
        postRender() {

            let page = this.model.get("page");
            let json = page.get("components").word;
            
            // Determing the TN: value in the wordBuilder based on the ID values.
            let tn = [
                page.get("name"),
                page.get("subName") ? ( "(" + page.get("subName") + ")" ) : ""
            ];
            tn = tn.join("");

            // Create a WordBuilder for each _item.
            _.each(json._items, (itemData) => {

                // Allow for the _header.tn value to be manually overridden.
                let data = _.extend({}, itemData);
                if (data._header && !data._header.tn) data._header.tn = tn;

                this.$(".word__widget").append(new WordBuilderView({
                    model: new Backbone.Model(data)
                }).$el);
            });
        }
    }

    CompWordView.template = "word";
    return CompWordView;
});