define([
    "dd",
    "core/js/views/coreView"
  ], function(DD, CoreView) {

    class WordBuilderView extends CoreView {

        className(){
            return "wordbuilder"
        }

        events() {
            return {
            }
        }

        init() {

        }

        postRender() {
            this.renderCopyBtns();
        }

        renderCopyBtns(){
            this.$(".wordbuilder__controls").append(new DD.CopyBtn(this.$(".wordbuilder__inner")).$el);

            let $itemContainers = this.$(".wordbuilder__item-container");
            $itemContainers.each((i, e) => {
                $(e).find(".wordbuilder__item-controls").append(new DD.CopyBtn($(e)).$el);
            });
        }
    }

    WordBuilderView.template = "wordBuilder";
    return WordBuilderView;
});