define([
    "dd",
    "core/js/views/componentView"
  ], function(DD, ComponentView) {

    /**
     * Shows preview layouts for the provided page.
     */

    const ADAPT_DEVICE_SMALL = 520;

    class ComponentPreviewView extends ComponentView {

        events(){
            return _.extend({}, super.events(), {
                "change .js-preview-screensize-radio" : "onRadioScreensizeChange",
                "change .js-preview-example-select" : "onExampleSelectChange",
                "click .js-preview-example-prev" : "onExamplePrevClick",
                "click .js-preview-example-next" : "onExampleNextClick",
            });
        }

        init(){
            
            // Fallback if no _examples values set.
            if (!this.model.get("_examples")) {
                this.model.set("_examples", [{
                    "title": "EXAMPLES TBA",
                    "_url": "placeholder"
                }]);
            }
            this.model.set("_exampleCount", this.model.get("_examples").length);

            super.init();
            this.setUpEventListeners();
        }

        setUpEventListeners(){
            this.onDeviceResize = _.debounce(this.onDeviceResize.bind(this),500);
            this.listenTo(DD, "device:resize", this.onDeviceResize);
            this.listenTo(this.model, "change:_screenSize", this.onScreenSizeChange);
            this.listenTo(this.model, "change:_exampleIdx", this.onExampleIdxChange);
        }

        onExampleSelectChange(event) {
            
            this.model.set("_exampleIdx", $(event.currentTarget).val());
        }

        onExamplePrevClick() {

            let exampleIdx = this.model.get("_exampleIdx");
            if (exampleIdx <= 0) return;
            
            this.model.set("_exampleIdx", exampleIdx - 1);
        }

        onExampleNextClick() {

            let exampleIdx = this.model.get("_exampleIdx");
            if (exampleIdx >= this.model.get("_exampleCount")-1) return;

            this.model.set("_exampleIdx", exampleIdx + 1);
        }

        onExampleIdxChange(model, exampleIdx) {

            this.$(".js-preview-example-select").val(exampleIdx);
            this.$(".js-preview-example-prev").prop("disabled", exampleIdx == 0);
            this.$(".js-preview-example-next").prop("disabled", exampleIdx == this.model.get("_exampleCount")-1);

            let example = this.model.get("_examples")[exampleIdx];

            let src = `${example._isHybrid ? "adapt-hybrid-preview" : "adapt-preview"}/index.html?debug=false&navigationEnabled=false&block=${example._url}`;

            console.log("---------------------------------------------" + src);

            this.$(".js-preview-iframe").attr("src", src);
        }

        /* Limit the max-width of the iframe if the preview is in phone mode. */
        onScreenSizeChange(model, screenSize) {
            this.$(".js-preview-iframe-container").css({
                "max-width" : screenSize == "phone" ? Math.round(ADAPT_DEVICE_SMALL * 0.67) : ""
            });
        }

        onRadioScreensizeChange(event) {
            this.model.set("_screenSize", $(event.currentTarget).val());
        }

        onDeviceResize(deviceWidth) {

            // Check if the current width applied to the iframe would force the Adapt content inside of it to enter its small layout.
            let width = $(".page__inner").width();

            // Disable clicking on the Desktop layout if the screen width would be too small to let the iframe expand to it.
            let phoneOnly = width < ADAPT_DEVICE_SMALL;
            this.$(".js-preview-screensize-radio-desktop").prop("disabled", phoneOnly);
            this.$(".js-preview-screensize-radio-phone").prop("checked", phoneOnly ? true : this.model.get("_screenSize") == "phone");
            this.$(".js-preview-screensize-radio-desktop").prop("checked", phoneOnly ? false : this.model.get("_screenSize") == "desktop");
        }

        postRender() {
            
            // Set up initial values.
            this.model.set("_screenSize", "desktop");
            this.model.set("_exampleIdx", 0);
            this.onDeviceResize();
        }
    }

    ComponentPreviewView.template = "preview";
    return ComponentPreviewView;
});