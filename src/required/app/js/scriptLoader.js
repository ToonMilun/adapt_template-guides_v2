// For any third party dependencies, like jQuery, place them in the lib folder.

requirejs.config({
    waitSeconds:    30,
    baseUrl: '',
    paths: {
        /*app:        '../app',
        templates:  '../app/templates',
        views:      '../app/views',
        models:     '../app/models',
        collections:'../app/collections',
        enums:      '../app/enums',
        appData:    '../app/appData',
        components: '../app/components',
        extensions: '../app/extensions',
        
        assets:     '../../assets',
        core:       '../app/core',*/

        libraries:  'libraries',

        dd:         'core/js/dd',

        jquery:     'libraries/jquery-3.4.1.min',
        underscore: 'libraries/lodash',
        backbone:   'libraries/backbone',
        text:       'libraries/text'
    },
    shim: {

        // Backbone
        "backbone": {
  
           // Depends on underscore/lodash and jQuery
           "deps": ["underscore", "jquery"],

           // Exports the global window.Backbone object
           "exports": "Backbone"
        }
    },
    catchError: true
});

// Start loading the main app file. Put all of
// your application logic in there.
requirejs([
    'core/js/main'
]);
