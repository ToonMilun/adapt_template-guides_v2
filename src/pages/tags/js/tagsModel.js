/*jshint -W027*/
define([
    "dd",
    "core/js/models/pageModel"
], function(DD, PageModel) {

    class TagsModel extends PageModel {

        defaults(){
            return {
                
            }
        }

        init() {
            // Convert DD.tags to an array for Handlebars to be able to render it.
            this.set("_tags", DD.tags.collection.toJSON());
        }

        /**
         * Used by the router.
         * @param {*} tagNames 
         */
        setActiveTagsByName(tagNamesArr){

            let tags = this.get("_tags");
            _.each(tags, (tag) => {
                if (_.find(tagNamesArr, (tagName) => {return tagName == tag.name;})) {
                    tag._isActive = true;
                }
            });
        }
    }

    return TagsModel;
});