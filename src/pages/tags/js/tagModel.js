define([
    "core/js/models/pageModel"
  ], function(PageModel) {

    class TagModel extends Backbone.Model {

      default() {
        return {
          "name": "",
          "desc": "",            // Description which will appear on mouseover.
          "_pages": undefined    // Collection of pageModels that have this tag.
        }
      }

      initialize() {
        this.set("_pages", new Backbone.Collection(null, {
          model: PageModel
        }));
      }

      addPage(pageModel) {
        this.get("_pages").add(pageModel);
      }
    }

    return TagModel;
});