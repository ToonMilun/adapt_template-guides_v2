/*jshint -W027*/
define([
    "dd",
    "core/js/views/pageView"
], function(DD, PageView) {


    /**
     * Page that displays all GuidePage tag information + GuidePage lookup.
     */

    class TagsView extends PageView {

        className(){
            return "page page-tags"
        }

        events(){
            return {
                "click .tag" : "onTagClick"
            }
        }

        init() {
            
        }

        postRender(){
            this.renderTags();
            this.renderThumbs();
            this.filterThumbs();
            this.refreshTags();
        }

        /**
         * Whenever a tag is clicked, toggle its active state.
         */
        onTagClick(event) {

            let tags = this.model.get("_tags");
            let $tag = $(event.currentTarget);
            let index = $tag.index();

            tags[index]._isActive = !tags[index]._isActive;
            
            this.refreshTags();
            this.filterThumbs();
        }

        refreshTags(){
            let tags = this.model.get("_tags");

            _.each(tags, (tag, index) => {
                this.$(".tag[data-tag='" + tag.name + "']").toggleClass("is-active", Boolean(tag._isActive));
            });
        }

        renderTags(){
            this.$(".tags-container").html(
                DD.tags.renderTags()
            );
        }
        
        /**
         * Render the thumbs for all pages.
         */
        renderThumbs(){

            let thumbsView = DD.pages.guides.renderThumbsView();

            let $par = this.$(".page-tags__results-thumbs");
            $par.html(thumbsView.$el);

            this.thumbsView = thumbsView;
        }

        /**
         * Show + hide the thumbs to show only those whos page tags match the active tags.
         */
        filterThumbs(){

            let activeTagNames = [];
            _.each(this.model.get("_tags"), (tag) => {
                if (tag._isActive) activeTagNames.push(tag.name);
            });

            // Show only thumbnails of pages which include all of the selected tags.
            let matchedPages = this.thumbsView.filterThumbs((pageModel) => {
                
                let allFound = true;
                _.find(activeTagNames, (tagName) => {
                    if (!_.includes(pageModel.get("tags"), tagName)) {
                        allFound = false;
                        return true;
                    }
                });
                return allFound;
            });

            // Update the totals.
            this.$(".page-tags__results-total").text(
                matchedPages.length + " of " + DD.pages.guides.collection.length + " pages matched the provided filters:"
            );
        }
    }

    TagsView.template = "tags";
    return TagsView;
});