define([
    "core/js/views/coreView"
  ], function(CoreView) {

    /**
     * Renders a single tag.
     */
    class TagView extends CoreView {

        className(){
            return "tag"
        }

        tagName(){
            return "span"
        }

        attributes(){
            return {
                "data-tag": this.model.get("name"),
                "title": this.model.get("desc")
            }
        }

        init() {

        }
    }

    TagView.template = "tag";
    return TagView;
});