/*jshint -W027*/
define([
    "dd",
    "core/js/views/pageView",
    "text!content/pageComponentTypes.json"
], function(DD, PageView, PageComponentTypes) {

    let PageComponentTypesJSON = JSON.parse(PageComponentTypes);

  /**
   * Page containing template information.
   */

  class GuideView extends PageView {

      className(){
          return "page --" + this.model.get("type")
      }

      events(){
          return {
              "click .tag" : "onTagClick"
          }
      }

      postRender() {
          this.renderTags();
          this.renderComponents();
      }

      renderTags() {
          this.$(".js-page-tags").html(
              DD.tags.renderTagsByNames(this.model.get("tags"))
          );
      }

      /**
       * FIXME: Hardcoded.
       */
      renderComponents() {
         
          _.each(this.model.get("components"), (component, key) => {

              let pageComponentData = PageComponentTypesJSON[key];
              if (pageComponentData._isEnabled === false) return;

              component = _.extend(component, {
                  type: key,
                  title: PageComponentTypesJSON[key].title,
                  page: this.model,
                  assetsURL: "content/pages/" + this.model.get("type") + "/" + this.model.get("_id") + "/"
              });
              
              //console.log("components/" + pageComponentData.view + "/js/" + pageComponentData.view);

              // Load and render each component's view.
              require(["components/" + pageComponentData.view + "/js/" + pageComponentData.view], (ComponentView) => {
                
                  this.$(".page__component-container[data-type='" + key + "']").append(new ComponentView({
                      model: new Backbone.Model(component)
                  }).$el);
              },
              () => {
                //DD.log.warn("Could not load view: " + "views/" + PageComponentTypesJSON[key].view);
              });
          });
      }

      /**
       * Whenever a tag is clicked, have it link to the tags page.
       * @param {*} event 
       */
      onTagClick(event){

          let tagName = $(event.currentTarget).data("tag");
          Backbone.history.navigate('#/tags/' + tagName, {trigger: true});
      }
  }

  GuideView.template = "page";
  return GuideView;
});