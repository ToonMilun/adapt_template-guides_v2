/*jshint -W027*/
define([
    "dd",
    "core/js/models/pageModel"
], function(DD, PageModel) {

    class GuideModel extends PageModel {

        defaults(){
            return {
                "_id": "ERROR",      // page's unique url.
                "_type": "",         // "template-common" | "template-uncommon" | "template-yt" | "template-activity" | "extra" | etc.

                "name": "ERROR",    // Presentable name of the page.

                /*"_parents": undefined,
                "_children": undefined  // Components*/
            }
        }

        init() {
            //console.log(this.attributes);

            this.initTags();

            // Modularity
            this.set("_parents", new Backbone.Collection(null, {model: GuideModel, comparator: 'name'}));
            this.set("_children", new Backbone.Collection(null, {model: GuideModel, comparator: 'name'}));
            
            // Wait for all GuidePage data to be loaded, then initialize modularity.
            /*this.listenTo(DD, "dd:dataLoaded", () => {
                this.initModularity();
            });*/
        }

        /**
         * Add this pages tags to the database.
         */
        initTags() {

            // Add the page's type as a tag.
            //this.get("tags").push(this.get("type"));

            _.each(this.get("tags"), (tag) => {
                DD.tags.registerPage(tag, this);
            });
        }

        /**
         * Create 2 collections, one for all template which can be children to this one,
         * and one for all parents this template can go in. 
         */
        initModularity() {
            
            let components = this.get("components");
            let guidePageCollection = DD.pages.guides;

            // Fallbacks
            // ---------------------------------------
            if (!components) {
                this.set("components", {});
                components = this.get("components");
            }
            if (!components.modularity) {
                components.modularity = {
                    parents: {
                        includeTags: [],
                        excludeTags: [],
                        includePages: [],
                        excludePages: []
                    }
                }
            }
            // ---------------------------------------

            let parents = components.modularity._parents;
            if (!parents) {
                DD.log.warn("components.modularity.parents have not been defined for page \"" + this.get("_id") + "\".");
                return;
            }

            // Include all pages with the specified tags, or all of them if the only tag is "all".
            let includeAll = false;
            if (parents._includeTags.length >= 1 && parents._includeTags[0]) includeAll = true;
            let parentPages = includeAll ? 
            guidePageCollection.models :
            guidePageCollection.getPagesContainingAllTagStrings(parents._includeTags);

            console.log("TBA: Remove manually specified tags.");

            // Add manually included pages.
            _.each(parents._includePages, (p) => {
                let page = guidePageCollection.getPageById(p);
                if (page) {
                    parentPages.push(page);
                }
                else {
                    DD.log.error("Page \"" + this.get("_id") + "\" has a parent which does not exist: \"" + p + "\".");
                }
            });

            // Remove manually excluded pages, and remove this GuideModel itself.
            parentPages = _.reject(parentPages, (GuideModel) => {
                return GuideModel == this || _.includes(parents._excludePages, GuideModel.get("_id"))
            });

            this.get("_parents").add(parentPages);

            // Add this page to all the parentPage's _children collections.
            _.each(this.get("_parents").models, (m) => {
                m.get("_children").add(this);
            });
        }
    }

    return GuideModel;
});