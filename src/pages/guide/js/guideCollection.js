define([
    "dd",
    "text!pageData.json",
    "./guideModel",
    "core/js/models/thumbsModel",
    "core/js/views/thumbsView",
    "pages/tags/js/tagsCollection" // GuidePages use tags, so load those in too.
  ], function(DD, PageData, PageGuideModel, ThumbsModel, ThumbsView) {

    let GuidePageCollection = new Backbone.Collection(null, { model: PageGuideModel, comparator: 'name' });
    let Pages = GuidePageCollection;

    //Pages.collection = GuidePageCollection;

    // Functions
    // ---------
    Pages.addPage = function(pageGuideModel) {
        GuidePageCollection.add(pageGuideModel);
    };

    Pages.getPageById = function(id){
        let page = _.find(GuidePageCollection.models, (pageGuideModel) => {
            return pageGuideModel.get("_id") == id;
        });

        if (!page) {
            DD.log.warn("Couldn't find pageGuideModel with id \"" + id + "\".");
            return undefined;
        }

        return page;
    }

    Pages.setPageActiveById = function(id){

        let targetPage = Pages.getPageById(id);
        _.each(GuidePageCollection.models, (pageGuideModel) => {
            pageGuideModel.set("_isActive", targetPage == pageGuideModel);
        });

        return targetPage;
    }

    Pages.getPagesByType = function(pageType){
        return GuidePageCollection.filter((page) => {

            let type = page.get("type");

            if (pageType == "page"){
                switch (type){
                    case "template-common":
                    case "template-uncommon":
                    case "template-activity":
                    case "template-yt":
                    case "extra":
                    case "hybrid":
                    case "hybrid-yt":
                    case "hybrid-extra":
                        return true;
                }
            }

            return page.get("type") == pageType;
        });
    }

    // Load in the data from PageData
    // ------------------------------
    PageData = JSON.parse(PageData);
    _.each(PageData.pages, (pageData, key) => {
        Pages.addPage(new PageGuideModel(_.extend(pageData, {_id: key})));
    });

    Pages.getPagesContainingAllTags = function(tagStringArr){

        if (!tagStringArr || tagStringArr.length == 0) return []; // Empty array returns nothing.

        return _.filter(GuidePageCollection.models, (page) => {
            return _.every(tagStringArr, (tagName)=>{
                return _.includes(page.get("tags"), tagName);
            });
        });
    }
    
    Pages.getPagesContainingAllTagStrings = function(tagStringArr){
        return Pages.getPagesContainingAllTags(tagStringArr);
    }

    Pages.getPageById = function(id){
        let page = _.find(GuidePageCollection.models, (pageModel) => {
            return pageModel.get("_id") == id;
        });

        if (!page) {
            DD.log.warn("Couldn't find pageModel with id \"" + id + "\".");
            return undefined;
        }

        return page;
    }

    /**
     * Renders all page thumbs and returns the view for appending.
     */
    Pages.renderThumbsView = function(options){
        return new ThumbsView({model: new ThumbsModel(
            _.extend({}, options, {_pages: GuidePageCollection})
        )});
    }

    DD.pages.guides = Pages;
});