define([
    "dd",
    "text!content/info.json",
    "./infoModel"
  ], function(DD, InfoData, InfoModel) {

    let InfoCollection = new Backbone.Collection(null, { model: InfoModel, comparator: 'name' });
    let Info = {};

    Info.collection = InfoCollection;

    DD.pages.info = Info;
});