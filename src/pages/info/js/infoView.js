/*jshint -W027*/
define([
    "dd",
    "core/js/views/pageView"
], function(DD, PageView) {


    /**
     * Page that displays generic text info.
     */

    class InfoView extends PageView {

        className(){
            return "page page-info"
        }

        events(){
            return {
                
            }
        }

        init() {
            
        }

        postRender(){
            
        }
    }

    InfoView.template = "info";
    return InfoView;
});