/*jshint -W027*/
define([
    "dd",
    "text!content/info.json",
    "text!pageData.json",
    "core/js/models/pageModel"
], function(DD, InfoData, PageData, PageModel) {

    const infoData = JSON.parse(InfoData);
    const pageData = JSON.parse(PageData);

    class InfoModel extends PageModel {

        defaults(){
            return {
                "pageName": "" // Value set by router.js.
            }
        }

        init() {
            //console.log(this.get("pageName"))
            //console.log(InfoData);

            this.getPageData();
        }

        getPageData() {

            let pageName = this.get("pageName");

            // Find the matching pageName across all _groups._items in the infoData.json.
            let groups = infoData._groups;
            let infoItem = undefined;
            
            _.find(groups, group => {
                infoItem = _.find(group._items, item => {
                    return item.name == pageName;
                });
                return Boolean(infoItem);
            });

            // Get the corresponding page from the pageData.
            let data = _.find(pageData.pages, (data, key) => {
                return key == infoItem.page;
            });

            this.set(data);
        }
    }

    return InfoModel;
});