define([
    "dd",
    "./accordionBtnModel",
    "./accordionBtnView"
  ], function(DD, AccordionBtnModel, AccordionBtnView) {

    class AccordionBtn {
        constructor($target, settings) {
            let model = new AccordionBtnModel(_.extend({$target: $target}, settings));
            let view = new AccordionBtnView({
                model: model
            });

            return view;
        }
    }
    
    DD.AccordionBtn = AccordionBtn;
    return DD.AccordionBtn;
});