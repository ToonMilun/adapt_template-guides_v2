define([
  ], function() {

    class AccordionBtnModel extends Backbone.Model {

        defaults(){
            return {
                $target: $(),                   // Target element that the copyBtn will copy when clicked.
                _isActive: false                // Whether the $target is visible or not.
            }
        }
    }

    return AccordionBtnModel;
});