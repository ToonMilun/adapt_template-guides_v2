define([
    "dd",
    "core/js/views/coreView"
  ], function(DD, CoreView) {

    class AccordionBtnView extends CoreView {

        className(){
            return "accordionbtn " + (this.model.get("_isActive") ? "is-active" : "");
        }

        events(){
            return {
                "click"     : "onClick"
            }
        }

        init() {
            this.setUpEventListeners();
            this.model.get("$target").addClass("is-accordionbtn-target");
        }

        setUpEventListeners() {
            this.listenTo(this.model, "change:_isActive", this.onActiveChange);
        }

        postRender() {
            // If the button starts off inactive; start its $target off hidden too.
            if (!this.model.get("_isActive")) this.model.get("$target").hide(0);
        }

        onActiveChange(model, isActive) {
            this.$el.toggleClass("is-active", isActive);

            if (isActive) {
                this.model.get("$target").slideDown();
            }
            else {
                this.model.get("$target").slideUp();
            }
        }

        onClick() {
            this.toggle();
        }

        toggle() {
            this.model.set("_isActive", !this.model.get("_isActive"));
        }
    }

    AccordionBtnView.template = "accordionBtn";
    return AccordionBtnView;
});