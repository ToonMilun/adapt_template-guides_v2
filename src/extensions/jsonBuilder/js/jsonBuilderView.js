define([
    "dd",
    "core/js/views/coreView",
    "./jsonBuilderPropView",
    "./jsonBuilderPropModel",
    'libraries/run_prettify'
  ], function(DD, CoreView, JsonBuilderPropView, JsonBuilderPropModel) {

    class JsonBuilderView extends CoreView {

        className(){
            let classes = [
                "jsonbuilder",
                "is-" + this.model.get("_type")
            ];
            return classes.join(" ");
        }

        events() {
            return {
            }
        }

        init() {
            
            // FIXME: Wonky placeholder implementation.
            // This is changed for helper prop views.
            //this.propViewTemplate = this.constructor.template != "jsonBuilder" ? "jsonBuilderHelperProp" : "jsonBuilderProp";
            this.propViewTemplate = (this.model.get("_type") === "helpers") ? "jsonBuilderHelperProp" : "jsonBuilderProp";
        }

        renderNormal() {

            JsonBuilderPropView.template = "jsonBuilderProp";
            let $jsonPropsContainer = this.$(".jsonbuilder__properties");

            // Render all properties.
            let root = this.model.get("_root");
            _.each(root, (val) => {
                $jsonPropsContainer.append(new JsonBuilderPropView({
                    model: val
                }).$el);
            });

            // Wrap all hidden items in <<<~ ~>>> tags.
            let $hidden = this.$(".jsonbuilder__prop.is-hidden");
            let $groups = [];

            // Find every group of hidden properties first.
            $hidden.each((i, e) => {

                let $group = $(e);

                if ($group.hasClass("is-grouped")) return true;

                $group = $group.add($(e).prevUntil(":not(.is-hidden)"));
                $group = $group.add($(e).nextUntil(":not(.is-hidden)"));
                $group.addClass("is-grouped");
                $groups.push($group);
            });

            // Then wrap them in the hidden tags.
            _.each($groups, ($e) => {
                let $wrapper = $("<div class='jsonbuilder__jsonsynt is-hidden'>");
                $e.wrapAll($wrapper);
                $wrapper = $e.parent();

                let $indents = $e.find(".jsonbuilder__prop-indents").first();
                $wrapper.prepend("<span class='jsonbuilder__prop-tag'><<<~</span>");
                $wrapper.prepend($indents.clone());
                $wrapper.append($indents.clone());
                $wrapper.append("<span class='jsonbuilder__prop-tag'>~>>></span>");
            });
        }

        /**
         * Reworked.
         * 06/09/2021
         */
        renderHelper() {

            JsonBuilderPropView.template = "jsonBuilderHelperProp";
            let $jsonPropsContainer = this.$(".jsonbuilder__properties");
            
            // Render all properties.
            let root = this.model.get("_root");
            let template = Handlebars.templates["jsonBuilderHelper"];

            _.each(root, (val) => {

                let content = val.getChild("_content").getVal();

                let data = {
                    name: val.getChild("_name").getVal(),
                    content: val.getChild("_content").getVal(),
                    _isBlock: val.getChild("_isBlock")
                }

                $jsonPropsContainer.append(Handlebars.compile(template)(data));

                let properties = val.getChild("_properties");

                properties.set("name", "");
                $jsonPropsContainer.find(".jsonbuilder__helper-properties").last().append(new JsonBuilderPropView({
                    model: properties
                }).$el);
            });

            // Populate a list of all possible properties this helper can have here.
            template = Handlebars.templates["jsonBuilderHelperPropList"];
            let $jsonPropsList = this.$(".jsonbuilder__helper-properties-list");
            let properties = this.model.get("_properties");
            let myProperties = this.model.get("_myProperties");

            // Filter the properties object (which has the definitions for ALL helper properties in it) to only
            // contain the properties from "myProperties".
            // Note: _.filter will convert properties to an array.
            properties = _.filter(properties, (val, key) => {
                if (myProperties[key] !== undefined) {
                    properties[key].name = key;
                    return true;
                }
                return false;
            });
            properties = _.sortBy(properties, (e) => {return e._isOptional ? 1 : -1;});
            $jsonPropsList.append(Handlebars.compile(template)(properties));
        }

        // Highlight + code prettify the example if there is one.
        renderExample() {

            let html = this.model.get("_example");
            if (!html) return;
            
            html = html.replace(/&lt;/gm, "<");
            html = html.replace(/&gt;/gm, ">");

            let $exampleInner = this.$(".jsonbuilder__example-inner");

            $exampleInner.text(html);
            html = $exampleInner.html();
            html = html.replace(/({{#(\w+).*?}}.*?{{\/(\2)}})/gm, (match) => {
                return "<span class='jsonbuilder__helper-mark'>" + match + "</span>";
            });
            $exampleInner.html(html);
            $exampleInner.html(PR.prettyPrintOne($exampleInner.html())).addClass("prettyprint");
        }
        
        postRender() {
            
            // If the type of JSON template being provided has type "helpers", then it needs to be rendered differently.
            let type = this.model.get("_type");
            let isHelper = type === "helper";
            let isHtml = type === "html";// || "other";
            let isOther = type === "other";

            if (isHelper) {
                this.renderHelper();
                this.renderToggleBtn();
            }
            else if (isHtml) {
                //this.renderExample();
                this.renderToggleBtn();
            }
            else if (isOther) {
                this.renderToggleBtn();
            }
            else {
                this.renderNormal();
                this.renderCopyBtn();
                this.renderExample();
                this.renderToggleBtn();
            }
        }

        renderCopyBtn(){
            this.$(".jsonbuilder__header-controls").first().append(new DD.CopyBtn(this.$(".jsonbuilder__json").first(), {_includeAdvanced: false}).$el);

            /*this.$(".jsonbuilder__header-controls").first().append(new DD.CopyBtn(this.$(".jsonbuilder__json").first(), {_includeAdvanced: true, _isCopyAll: true}).$el);*/
        }

        renderToggleBtn() {
            this.$(".jsonbuilder__header-controls").first().append(new DD.AccordionBtn(this.$(".jsonbuilder__json").first()).$el);
        }
    }

    JsonBuilderView.template = "jsonBuilder";
    return JsonBuilderView;
});