define([
    "./jsonBuilderPropModel"
  ], function(JsonBuilderPropModel) {

    /**
     * An app that will allow developers to look up the definitions of properties & customize their Adapt JSONSYNT templates.
     * Each individual JSON code snippet on a page should have it's own JsonBuilderModel + View.
     */

    class JsonBuilderModel extends Backbone.Model {

      defaults() {
        return {
          _root: undefined,   // Root JsonBuilderPropModel(s) which contains all others.
          _properties: {}     // Descriptions of the properties found in _root.
        }
      }

      initialize() {

        let root = this.get("_root");
        let _root = [];

        // Allow the root element to not be an array.
        if (!Array.isArray(root)) {
          root = [root];
        }

        // Skip making the outer root wrapper (the blank "{" and "}").
        if (this.get("_noRoot")) {
          _.each(root, (r) => {

            _.each(r, (val, key) => {
              _root.push(new JsonBuilderPropModel({
                name: key,
                value: val,
                _assetsPath: this.get("_assetsPath"),
                _properties: this.get("_properties") // Descriptions for all properties that may appear in this JSON.
              }));
            });
          });
          this.set("_root", _root);
        }
        // Create a JsonBuilderPropModel for each of root element.
        else {
          _.each(root, (r) => {
            _root.push(new JsonBuilderPropModel({
              name: "",
              value: r,
              _assetsPath: this.get("_assetsPath"),
              _properties: this.get("_properties") // Descriptions for all properties that may appear in this JSON.
            }));
          });
          this.set("_root", _root);
        }
      }
    }

    return JsonBuilderModel;
});