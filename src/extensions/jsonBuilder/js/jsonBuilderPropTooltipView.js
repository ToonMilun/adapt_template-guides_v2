define([
  "core/js/views/coreView"
], function(CoreView) {

  class JsonBuilderPropTooltipView extends CoreView {

      className(){
          let classes = [
            "jsonbuilder__prop-tooltip",
            (this.model.get("_isHidden") ? "is-hidden" : ""),
            (this.model.get("_isOptional") ? "is-optional" : ""),
            (this.model.get("_isNotImplemented") ? "is-notimplemented" : "")
          ];
          return classes.join(" ");
      }

      init() {
        
      }

      postRender() {
          
          
      }
  }
  
  JsonBuilderPropTooltipView.template = "jsonBuilderPropTooltip";
  return JsonBuilderPropTooltipView;
});