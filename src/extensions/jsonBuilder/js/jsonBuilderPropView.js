define([
    "core/js/views/coreView",
    "./jsonBuilderPropTooltipView"
  ], function(CoreView, JsonBuilderPropTooltipView) {

    class JsonBuilderPropView extends CoreView {

        className(){
            let classes = [
                "jsonbuilder__prop",
                (this.model.get("_isHidden") ? "is-hidden" : ""),
                (this.model.get("_isAdvanced") ? "is-advanced" : ""),
                (this.model.get("_variations") ? "has-variations" : "")
            ]
            return classes.join(" ");
        }

        events() {

            let e = {};

            // If this property has variations, add click events to them.
            if (this.hasVariations()) {
                e["click .js-jsonbuilder-prop-variations-item"] = "onVariationsItemClick";
            }
            
            return e;
        }

        attributes() {

            let name = this.model.get("name");
            if (!name && this.model.get("_parent")) name = this.model.get("_parent").get("name") + "-child"; 
            return {
                "data-name" : name,
                "data-fullname": this.model.get("_fullname")
            }
        }

        hasVariations() {
            return Boolean(this.model.get("_variations"));
        }

        init() {
            
            if (this.hasVariations()) {
                this.listenTo(this.model, "change:_variationIdx", this.onVariationIdxChange);
                
                // Start off with the first variation active.
                this.model.setVariation(0);
            }
        }

        
        onVariationIdxChange(model, idx) {
            this.reRender();
        }

        onVariationsItemClick(event) {

            let idx = $(event.currentTarget).attr("data-index");
            this.model.setVariation(idx);
        }

        postRender() {

            // Create the tooltip.
            let tooltip = this.model.get("_tooltip");
            this.$(".jsonbuilder__prop-name").append(new JsonBuilderPropTooltipView({
                model: tooltip || new Backbone.Model({})
            }).$el);

            // Create and append views for all children (if this property has them).
            let children = this.model.get("_children");
            if (!children) return;
            
            this.$(".jsonbuilder__prop-value").html("");
            children.each((childProp) => {

                // The .first() is very important (otherwise the children added would be targetted for appending too).
                this.$(".jsonbuilder__prop-value").first().append( 
                    new JsonBuilderPropView({model: childProp}).$el
                );
            });
        }
    }

    JsonBuilderPropView.template = "jsonBuilderProp";
    return JsonBuilderPropView;
});