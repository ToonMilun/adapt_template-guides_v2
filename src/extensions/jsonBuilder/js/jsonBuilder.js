define([
    "dd",
    "./jsonBuilderModel",
    "./jsonBuilderView"
  ], function(DD, JSONBuilderModel, JSONBuilderView) {

    // FIXME: Is this the correct way of creating a reuseable view creator?
    class JSONBuilder {
        constructor(settings) {

            let model = new JSONBuilderModel(_.extend(settings));
            let view = new JSONBuilderView({
                model: model
            });

            return view;
        }
    }
    
    DD.JSONBuilder = JSONBuilder;
    return DD.JSONBuilder;
});