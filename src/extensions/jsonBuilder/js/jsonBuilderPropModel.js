define([
    "backbone"
  ], function(Backbone) {

    /**
     * A JSON builder item is used for each property the JSON template contains.
     */

    class JsonBuilderPropModel extends Backbone.Model {

      defaults() {
        return {
          "name":         "! PROPERTYNAME !",
          "value":        "! PROPERTY VALUE !",
          "_assetsPath":  "", // Directory of the page.json that this property belongs to.
          "_properties":  {}, // Reference to a dictionary object containing definitions for all properties.

          "_fullname": "",  // propname.propname.propname
          "_type": false,   // Type of the value (array | object | string | int | etc.).
          "_tooltip": undefined,
          
          "_indent": 0,     // How many tab indents.
          "_decorativeIndent": 0,
          "_decorativeIndentPost": 0,
          "_isLast": false, // If true, then a , won't be added after this property. Set by the parent.
          "_isHidden": false, // If true, will be rendered very faded (it's a property that Adapt needs, but I don't want the developers touching).

          "_isError": false,          // If true, will be shown in bright red (the property hasn't been defined for the guides).
          "_isOptional": false,       // If true, will be rendered with a different colour to indicate it's not mandatory.
          "_isAdvanced": false,       // If true, will be excluded from the default copy+paste feature.
          "_isNotImplemented": false, // If true, will be rendered with a different colour to indicate the property isn't ready for use yet.

          "_parent":      undefined,  // If this is a property inside of a property, reference the parent property.
          "_children":    undefined   // Any child properties in the JSON.
        }
      }

      initialize() {
        
        this.setupData();

        this.listenTo(this, "change:_variationIdx", this.onVariationIdxChange);
      }

      setupData() {
        // If this model's value is an Array or an Object, recur through it and make child properties.
        this.initType();
        this.initChildren();
        this.initFullname();
        this.initDescription();
        this.initValue();

        this.initIndent();
        this.initChildrenDecorativeIndent();
      }

      /**
       * If this PropModel has variations, its children need to be updated whenever said variation changes.
       */
      setVariation(idx) {

        if (!this.get("_variations")) return;
        this.set("_variationIdx", idx);
      }

      onVariationIdxChange(model, idx) {

        let variations = this.get("_variations");
        let variation = variations[idx];

        this.set("value", variation._content);

        // Update the _variations array to indicate which one is active.
        variations.forEach((e, i) => {
          e._isActive = i == idx;
        });
        this.set("_variations", variations);

        this.setupData();
      }

      /**
       * Decorative tabbing. Make sure all the child values line up in a nice column ("longstrings" are exempt from this).
       */
      initChildrenDecorativeIndent() {

        let children = this.get("_children");
        if (!children) return;

        const tabSize = 4;

        let hasDecorativeIndent = function(childModel) {
          let type = childModel.get("_type");
          if (type == "longstring") return false;
          if (type == "prestring") return false;
          if (type == "array") return false;
          if (type == "object") return false;

          return true;
        }

        let maxNameLength = 0;
        children.each((child) => {
          if (!hasDecorativeIndent(child)) return;
          let l = child.get("name").length + tabSize; // '"" ' <- 3 extra characters.
          if (l > maxNameLength) maxNameLength = l;
        });
        maxNameLength = maxNameLength + tabSize - (maxNameLength % tabSize);

        // Set the decorative indent.
        children.each((child) => {
          if (!hasDecorativeIndent(child)) return;
          child.set("_decorativeIndent", Math.ceil((maxNameLength - (child.get("name").length + tabSize))/tabSize));
          //console.log(child.get("_decorativeIndent"));
        });

        // Set up the _decorativeIndentPost (for if this property has a _params JSONSYNT comment).
        //if (!this.get("_params")) return;
        let maxValueLength = 0;
        children.each((child) => {
          if (!child.get("_decorativeIndent")) return;
          let l = child.get("value").toString().length + (child.get("_type") == "string" ? tabSize : 0) + (child.get("_isLast") ? 3 : 0);
          if (l > maxValueLength) maxValueLength = l;
        });
        maxValueLength = maxValueLength + tabSize - (maxValueLength % tabSize);
        children.each((child) => {
          if (!child.get("_decorativeIndent")) return;
          child.set("_decorativeIndentPost", Math.ceil((maxValueLength - (child.get("value").toString().length + tabSize + (child.get("_isLast") ? 1 : 0)))/tabSize));
        });
      }

      initType() {
        this.set("_type", this.getObjectType(this.get("value")));

        let type = this.get("_type");
        if (type == "longstring" || type == "prestring") {
          let val = this.get("value");
          val = val.replace(/\s*(<<<|PPP)([\s\S]*?)(>>>|PPP)\s*/gm, (match, $1, $2, $3) => {
            return $2;
          });
          this.set("value", val);
          return;
        }
      }

      initChildren() {
      
        let type = this.get("_type");
        if (type !== "array" && type !== "object") return; // This object doesn't have children/

        // For each child, create a PropModel.
        let children = [];
        let _isHidden = this.get("_isHidden"); // If a Prop is hidden, its children should be hidden too.
        _.each(this.get("value"), (val, key) => {

          // Custom conditions 1:
          // Children with a name starting with <<<~ or ~>>> aren't real children. They're placeholders for the JSONSYNT "hidden" tags.
          if (typeof key === "string"){
            if (key.indexOf("<<<~") === 0) {
              _isHidden = true;
              return;
            }
            if (key.indexOf("~>>>") === 0) {
              _isHidden = false;
              return;
            }
          }

          children.push(new JsonBuilderPropModel({
            name: type === "array" ? "" : key,
            value: val,
            _assetsPath: this.get("_assetsPath"),
            _isHidden: _isHidden,
            _properties: this.get("_properties"),
            _parent: this
          }));
        });
        this.unset("value");

        // Bugfix: remove blank properties (they seem to be generated for objects automatically).
        children = _.filter(children, (child) => {
          return !(child.get("name") == undefined && child.get("_type") == undefined);
        });

        if (!children.length) return;
        children = new Backbone.Collection(children);

        children.last().set("_isLast", true);
        this.set("_children", children);
      }

      initIndent(){
        let indent = 0;
        let parent = this.get("_parent");

        while(parent) {
          indent++;
          parent = parent.get("_parent");
        }

        this.set("_indent", indent);
      }

      /**
       * Fullname is used when reading descriptions from jsonTemplateProperties.
       */
      initFullname(){
        let fullname = this.get("name");
        let parent = this.get("_parent");

        while(parent) {
          if (parent.get("name")){
            fullname = parent.get("name") + "." + fullname;
          }
          parent = parent.get("_parent");
        }

        this.set("_fullname", fullname);
      }

      /**
       * If the value is a string/longstring, highlight all instances "X" (it's used to indicate values which need to change). 
       */
      initValue() {
        
        let type = this.get("_type");
        if (type != "string" && type != "longstring"  && type != "prestring") return;

        /*return;

        let val = this.get("value");
        val = val.replace(/(X+)/g, (match, $1) => {
          return "<span class='jsonbuilder__placeholder'>" + $1 + "</span>"
        });
        this.set("value", val);*/
      }

      initDescription() {

        let fullname = this.get("_fullname");
        if (!fullname.trim()) return;

        let properties = this.get("_properties");
        let longestKey = "";

        // Find the _properties object with the key that matches this propertie's fullname the most.
        _.find(properties, (desc, key) => {
          if (fullname.endsWith(key) && key.length > longestKey.length) {
            longestKey = key;
          }
        });

        // Definition not found. Mark the property as one that needs to be defined.
        if (!longestKey) {
          this.set("_isError", true);
          return;
        }

        let desc = properties[longestKey];
        desc.name = longestKey;
        desc._isHidden = this.get("_isHidden");
        desc._type = this.get("_type");
        this.set("_tooltip", new Backbone.Model(desc));
        this.set("_params", desc._params);
        this.set("_comment", desc._comment == "inherit" ? desc._desc : desc._comment);
        this.set("_isOptional", desc._isOptional);
        this.set("_isAdvanced", desc._isAdvanced);
        this.set("_variations", desc._variations);
        this.set("_isNotImplemented", desc._isNotImplemented);
        this.set("_header", desc._header);
      }

      getObjectType(obj) {
        if (Array.isArray(obj)) return "array";
        let type = typeof obj;
        if (type === 'object' && obj !== null) return "object";
        if (type === 'string') {
          // Check if this is a longstring (custom type that means the string will be displayed like this <<<"<p>XXX</p>">>> instead).
          if (obj.match(/\s*<<<([\s\S]*)>>>\s*/gm)) {
            type = "longstring";
          }
          // Check if this is a prestring (custom type that means the string will be displayed like this <<<P<p>XXX</p>P>>>, and WITHOUT indents instead).
          if (obj.match(/\s*PPP([\s\S]*)PPP\s*/gm)) {
            type = "prestring";
          }
        }

        return type;
      }

      getChildren() {
        return this.get("_children");
      }

      getVal() {
        return this.get("value");
      }

      getChild(name) {
        return this.get("_children").find((child) => {
          return child.get("name") === name;
        });
      }
    }

    return JsonBuilderPropModel;
});