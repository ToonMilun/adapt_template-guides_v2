define([
    "dd",
    "core/js/views/coreView",
    "libraries/clipboard.min"
  ], function(DD, CoreView, ClipboardJS) {

    class CopyBtnView extends CoreView {

        className(){
            return "copybtn not-copy " + (this.model.get("_isCopyAll") ? "is-copyall" : "");
        }

        events(){
            return {
                "mouseenter": "onMouseEnter",
                "mouseleave": "onMouseLeave",
                "click"     : "onClick"
            }
        }

        init() {
            this.initModelData();
            this.model.get("$target").addClass("is-copybtn-target").toggleClass("is-copyall", this.model.get("_isCopyAll"));
        }

        postRender() {
            this.initClipboardJS();
        }

        // https://stackoverflow.com/questions/3169786/clear-text-selection-with-javascript
        clearSelection(){
            if (window.getSelection) {
                if (window.getSelection().empty) {  // Chrome
                    window.getSelection().empty();
                } else if (window.getSelection().removeAllRanges) {  // Firefox
                    window.getSelection().removeAllRanges();
                }
            } else if (document.selection) {  // IE?
                document.selection.empty();
            }
        }

        // 3rd party plugin.
        // Will automatically add an event to this view's element upon being initialized that will trigger a copy on click.
        initClipboardJS(){
            new ClipboardJS(this.$el[0], {
                target: (trigger) => {

                    let $target = this.model.get("$target");
                    let $clone = $target.clone();

                    // Convert all <mark> elements to <span class='mark'> (Word doesn't recognise <mark>).
                    // FIXME: Quick fix.
                    $clone.find("mark").each(function(){
                        $(this).replaceWith($("<span class='mark'>").html($(this).html()));
                    }); 

                    // Hide any non-copyable elements and  in the target first.
                    let $notCopy = $clone.find(".not-copy").remove();

                    // Remove any .is-advanced elements if applicable.
                    if (!this.model.get("_includeAdvanced")) {
                        let $advanced = $clone.find(".is-advanced").remove();
                    }

                    // Make sure the clone isn't visible while being copied.
                    // (In earlier versions, this was done by giving it a height of 0; but recent browser updates have made this stop working).
                    $clone.css({
                        "position": "absolute",
                        "transform": "translateX(100vw)"
                    });

                    $target.after($clone);
                    _.delay(() => {$clone.remove();}, 0);

                    return $clone[0];
                }
            });
        }

        initModelData() {
            
        }

        onClick() {
            _.debounce(this.playShutterAnimation.bind(this))();
        }

        // "Blinks" the element being copied to indicate that something has happened (purely a visual effect).
        playShutterAnimation() {
            this.$el.hide(0);
            this.model.get("$target").hide(0);

            this.$el.fadeIn();
            this.model.get("$target").fadeIn();

            this.clearSelection();
        }

        onMouseEnter() {
            this.model.get("$target").toggleClass("is-hover", true);
        }
        onMouseLeave() {
            this.model.get("$target").toggleClass("is-hover", false);
        }
    }

    CopyBtnView.template = "copyBtn";
    return CopyBtnView;
});