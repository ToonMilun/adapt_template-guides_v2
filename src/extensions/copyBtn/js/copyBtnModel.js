define([
  ], function() {

    class CopyBtnModel extends Backbone.Model {

        defaults(){
            return {
                $target: $(),                   // Target element that the copyBtn will copy when clicked.
                _shouldOutlineTarget: true,     // Whether to outline the target on mouseover.
                _isCopyAll: false,              // Will change colour of the button.
                _includeAdvanced: false         // If false, will remove all ".is-advanced" elements from the copy.
            }
        }
    }

    return CopyBtnModel;
});