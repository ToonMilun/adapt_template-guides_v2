define([
    "dd",
    "./copyBtnModel",
    "./copyBtnView"
  ], function(DD, CopyBtnModel, CopyBtnView) {

    // FIXME: Is this the correct way of creating a reuseable view creator?
    class CopyBtn {
        constructor($target, settings) {
            let model = new CopyBtnModel(_.extend({$target: $target}, settings));
            let view = new CopyBtnView({
                model: model
            });

            return view;
        }
    }
    
    DD.CopyBtn = CopyBtn;
    return DD.CopyBtn;
});