define(function() {

    // FIXME: Not actually an enum.
    var LOG_LEVEL = {
        "SUCCESS": "color: #008800;",
        "SCORM": "color: #3175bc;",
        "DEBUG": "color: #880088;",
        "WARN": "color: #ff8800;",
        "ERROR": "color: #ff0000;",
        "MESSAGE": "background-color: #ff8800; padding: 2px; color: white;"
    };

    return LOG_LEVEL;
})