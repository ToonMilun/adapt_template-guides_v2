define([
    "dd",
    "core/js/views/coreView",
    "core/js/views/thumbView"
  ], function(DD, CoreView, ThumbView) {

    /**
     * Renders all page thumbnails with headers.
     * Can be filtered.
     */
    class ThumbsView extends CoreView {

        className(){
            return "thumbs thumbs-" + this.model.get("size")
        }

        postRender() {
            this.renderThumbs();
        }

        events() {
            return {
                "click .thumbs__type-title" : "onTypeTitleClick"
            }
        }

        /**
         * Render the thumbs for all pages.
         */
        renderThumbs(){

            let $types = this.$(".thumbs__type");
            $types.hide(); // Only show types which have children.

            this.model.get("_pages").each((page) => {
                
                let $type = $types.filter("[data-type='" + page.get("type") + "']");
                $type.show();

                $type.find(".thumbs__type-items").append(new ThumbView({
                    model: page
                }).$el);
            });
        }

        /**
         * Show + hide the thumbs to show only those whos pageModel passes the predicate test.
         */
        filterThumbs(predicate){

            let matchedPages = _.filter(this.model.get("_pages").models, predicate);
            this.filterByPages(matchedPages);

            return matchedPages;
        }

        filterByPages(pageArr) {

            // Hide all thumbs and thumb type containers initially.
            this.$(".thumb, .thumbs__type").hide();

            // Show valid thumbs + unhide their type containers.
            _.each(pageArr, (pageModel) => {
                this.$(".thumb[data-id=" + pageModel.get("_id") + "]").show().closest(".thumbs__type").show();
            });

            return pageArr;
        }

        /**
         * To save space, the thumbs will hide themselves in the main navbar.
         * @param {*} event 
         */
        onTypeTitleClick(event) {

        }
    }

    ThumbsView.template = "thumbs";
    return ThumbsView;
});