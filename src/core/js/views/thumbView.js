define([
    "dd",
    "core/js/views/coreView"
  ], function(DD, CoreView) {

    /**
     * Renders a thumbnail for the provided page.
     */
    class ThumbView extends CoreView {

        className(){
            let classes = [
                "thumb",
                "--" + this.model.get("type"),
                (this.model.get("_isHidden") ? "is-hidden" : ""),
                (this.model.get("_isEnabled") === false ? "is-disabled" : "")
            ];
            return classes.join(" ");
        }

        attributes(){
            return {
                "data-id": this.model.get("_id")
            }
        }

        events(){
            /*return {
                "click" : "onClick"
            }*/
        }

        init() {
            
        }

        /**
         * If the thumb is clicked, have it link to the corresponding page.
         * @param {*} event 
         */
        onClick(event){
            Backbone.history.navigate('#/guide/' + this.model.get("_id"), {trigger: true});
        }
    }

    ThumbView.template = "thumb";
    return ThumbView;
});