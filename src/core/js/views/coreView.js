define([
  ], function() {

    /**
     * Basic view for all other views to inherit. Based on Adapt's adaptView.js implementation.
     */
    class CoreView extends Backbone.View {

        initialize() {
            this.init();
            this.preRender();
            this.render();
        }

        init(){}
        preRender(){}
        postRender(){}

        render() {
            if (!this.model) {
                throw "Model for coreView using template \"" + this.constructor.template + "\" is undefined!"; 
            }

            let data = this.model.toJSON();
            data.view = this;
            let template = Handlebars.templates[this.constructor.template];

            if (!template) {
                throw "Could not find template: \"" + this.constructor.template + "\"!"; 
            }
            
            this.$el.html(Handlebars.compile(template)(data));

            this.postRender();

            return this;
        }

        reRender() {
            this.render();
        }

    }

    return CoreView;
});