define([
  "dd",
  "core/js/views/coreView",
  "text!content/pageComponentTypes.json"
], function(DD, CoreView, PageComponentTypes) {

  let PageComponentTypesJSON = JSON.parse(PageComponentTypes);

  /**
   * Page containing template information.
   */

  class PageView extends CoreView {

      className(){
          return "page --" + this.model.get("type")
      }

      events(){
          return {
              "click .tag" : "onTagClick"
          }
      }

      postRender() {
          this.renderTags();
          this.renderComponents();
      }

      renderTags() {
          this.$(".js-page-tags").html(
              DD.tags.renderTagsByNames(this.model.get("tags"))
          );
      }

      /**
       * FIXME: Hardcoded.
       */
      renderComponents() {
         
          _.each(this.model.get("components"), (component, key) => {

              let pageComponentData = PageComponentTypesJSON[key];
              if (pageComponentData._isEnabled === false) return;

              component = _.extend(component, {
                  type: key,
                  title: PageComponentTypesJSON[key].title,
                  page: this.model,
                  assetsURL: "content/pages/" + this.model.get("type") + "/" + this.model.get("_id") + "/"
              });
              
              //console.log("components/" + pageComponentData.view + "/js/" + pageComponentData.view);

              // Load and render each component's view.
              require(["components/" + pageComponentData.view + "/js/" + pageComponentData.view], (ComponentView) => {
                
                  this.$(".page__component-container[data-type='" + key + "']").append(new ComponentView({
                      model: new Backbone.Model(component)
                  }).$el);
              },
              (err) => {
                throw err;
              });
          });
      }

      /**
       * Whenever an tag is clicked, have it link to the tags page.
       * @param {*} event 
       */
      onTagClick(event){

          let tagName = $(event.currentTarget).data("tag");
          Backbone.history.navigate('#/tags/' + tagName, {trigger: true});
      }
  }

  PageView.template = "page";
  return PageView;
});