define([
    "dd",
    "core/js/views/coreView"
  ], function(DD, CoreView) {

    /**
     * Core structure of the page (other views rendered inside of it).
     */
    class MainView extends CoreView {

      className() {
        return "main"
      }
    }

    MainView.template = "main";
    return MainView;
});