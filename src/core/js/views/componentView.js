/*jshint sub:true*/ // Ignore dot notation error for JSON hint.
define([
    "dd",
    "core/js/views/coreView",
    "libraries/jquery.cookie"
  ], function(DD, CoreView) {

    /**
     * If a component with a specific type is toggled to be hidden;
     * continue hiding that component when the user navigates between pages.
     * Also stores the value in cookies.
     * 
     * FIXME: Not the best way to implemnent this, but should do for now.
     */
    class ComponentSettingsSingleton extends Backbone.Model {

        defaults() {
            return {
                _cookieName: "adapt-template-guides-v2__componentSettings",
                _cookieExpires: 365,

                _componentTypesActive: {} // Dictionary of booleans.
            };
        }

        initialize() {
            this.loadDataFromCookie();
        }

        isComponentTypeActive(type) {

            let isActive = this.get("_componentTypesActive")[type];
            if (isActive === undefined) isActive = true;
            return isActive;
        }

        setComponentTypeActive(type, isActive) {
            let cta = this.get("_componentTypesActive");
            cta[type] = isActive;

            this.storeDataInCookie();
        }

        loadDataFromCookie() {
            let data = $.cookie(this.get("_cookieName"));
            if (!data) return;
            this.set("_componentTypesActive", JSON.parse(data.replace(/'/g,'"')));
        }
        
        storeDataInCookie() {
            $.cookie(
                this.get("_cookieName"),
                JSON.stringify(this.get("_componentTypesActive")).replace(/"/g,"'"),
                {expires: this.get("_cookieExpires")}
            );
        }
    }
    let componentSettings = new ComponentSettingsSingleton();

    // ----------------------------------------------------------------

    class ComponentView extends CoreView {
        
        className(){

            let classes = [
                "component",
                "component-" + this.model.get("type"),
                this.model.get("_isActive") ? "is-active" : ""
            ];

            return classes.join(" ");
        }

        events(){
            return {
                "click .js-component-toggle" : "onToggleClick"
            }
        }

        init() {
            this._setUpEventListeners();
            this._setUpModelData();
        }

        _setUpEventListeners() {
            this.listenTo(this.model, "change:_isActive", this.onActiveChange);
        }

        _setUpModelData() {
            this.model.set("_isActive", componentSettings.isComponentTypeActive(this.model.get("type")));
        }

        onToggleClick() {
            this.toggle();
        }

        toggle() {
            this.model.set("_isActive", !this.model.get("_isActive"));
        }

        onActiveChange(model, isActive) {
            this.$el.toggleClass("is-active", isActive);
            componentSettings.setComponentTypeActive(this.model.get("type"), isActive);

            if (isActive) {
                this.$(".js-component-body").slideDown();
            }
            else {
                this.$(".js-component-body").slideUp();
            }
        }

        render() {
            
            if (!this.model) {
                throw "Model for coreView using template \"" + this.constructor.template + "\" is undefined!"; 
            }

            let data = this.model.toJSON();
            data.view = this;
            let template = Handlebars.templates["component"];
            this.$el.html(Handlebars.compile(template)(data));

            let innerTemplate = Handlebars.templates[this.constructor.template];

            this.$(".component__body").html(Handlebars.compile(innerTemplate)(data)).wrapInner($("<div>", {class: this.model.get("type")}));

            this.postRender();

            return this;
        }
    }

    return ComponentView;
});