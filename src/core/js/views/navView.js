define([
    "dd",
    "core/js/views/coreView"
  ], function(DD, CoreView) {

    /**
     * Sidenav
     */

    class NavView extends CoreView {

      events() {
        return {
          "click .js-nav-controls-thumbnails" : "onControlsThumbnailsClick",
          "click .js-nav-controls-adapt" : "onControlsAdaptClick",
          "click .js-nav-controls-hyb" : "onControlsHybClick",
        }
      }

      className(){
        let classes = [
          "nav",
          this.model.getControls().get("_thumbsEnabled") ? "" : "hide-thumbs",
          this.model.getControls().get("_showAdapt") ? "" : "hide-adapt",
          this.model.getControls().get("_showHyb") ? "" : "hide-hyb",
        ];

        return classes.join(" ");
      }

      init() {
        this.listenTo(this.model.getControls(), {
          "change:_thumbsEnabled": this.onControlsThumbsEnabledChange,
          "change:_showAdapt": this.onControlsShowAdaptChange,
          "change:_showHyb": this.onControlsShowHybChange,
        });
      }

      postRender() {
        this.$(".nav__inner").append(DD.pages.guides.renderThumbsView().$el);
      }

      onControlsThumbsEnabledChange(model, isEnabled) {
        this.$el.toggleClass("hide-thumbs", !isEnabled);
      }

      onControlsShowAdaptChange(model, isEnabled) {
        this.$el.toggleClass("hide-adapt", !isEnabled);
      }

      onControlsShowHybChange(model, isEnabled) {
        this.$el.toggleClass("hide-hyb", !isEnabled);
      }

      onControlsThumbnailsClick(event) {
        let checked = $(event.currentTarget).is(":checked");
        this.model.getControls().set("_thumbsEnabled", checked);
      }

      onControlsAdaptClick(event) {
        let checked = $(event.currentTarget).is(":checked");
        this.model.getControls().set("_showAdapt", checked);
      }

      onControlsHybClick(event) {
        let checked = $(event.currentTarget).is(":checked");
        this.model.getControls().set("_showHyb", checked);
      }
    }

    NavView.template = "nav";
    return NavView;
});