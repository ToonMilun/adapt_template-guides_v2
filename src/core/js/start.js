define([
    "dd",
    "core/js/helpers",
    "core/js/views/mainView",
    "core/js/views/navView",
    "core/js/models/navModel"
], function (DD, Helpers, MainView, NavView, NavModel) {

    /**
     * Initialize and render everything that only ever needs to be done once per browser tab.
     */

    class Start extends Backbone.Model {
        
        async initialize() {

            // Load all page data.
            await DD.awaitLoad("pages/guide/js/guideCollection");

            this.render();
        }

        render() {
            $("body").html(new MainView({model: new Backbone.Model()}).$el);
            $("body").append(new NavView({model: new NavModel()}).$el);

            DD.trigger("dd:dataLoaded");
            require(['core/js/router'], (Router) => {
                new Router();
            });

        }
    }

    return Start;
});
