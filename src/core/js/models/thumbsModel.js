define([
  ], function() {

    class ThumbsModel extends Backbone.Model {

      default() {
        return {
          "_pageTypes": [], // template, template-yt, extras, other
          "size": "normal"
        }
      }

      initialize() {
        this.initTemplateTypes();
      }

      // FIXME: Hardcoded.
      initTemplateTypes() {

        let pageTypes = [
          {
            "type": "template-common",
            "name": "Templates - Common"
          },
          {
            "type": "template-uncommon",
            "name": "Templates - Uncommon"
          },
          {
            "type": "template-activity",
            "name": "Templates - Activities"
          },
          {
            "type": "template-yt",
            "name": "Templates - Your Turns"
          },
          {
            "type": "extra",
            "name": "Extras"
          },
          {
            "type": "hybrid",
            "name": "Hybrid Adapt - Templates"
          },
          {
            "type": "hybrid-yt",
            "name": "Hybrid Adapt - Your Turns"
          },
          {
            "type": "hybrid-extra",
            "name": "Hybrid Adapt - Extras"
          },
          {
            "type": "hybrid-table",
            "name": "Hybrid Adapt - Tables"
          }
        ];

        this.set("_pageTypes", pageTypes);
      }
    }

    return ThumbsModel;
});