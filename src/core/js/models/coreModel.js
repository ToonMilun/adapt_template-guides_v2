define([
  ], function() {

    /**
     * Basic model for most other models to inherit.
     * May not even be necessary.
     */
    class CoreModel extends Backbone.Model {

        initialize() {
            this.init();
        }

        init() {}
    }

    return CoreModel;
});