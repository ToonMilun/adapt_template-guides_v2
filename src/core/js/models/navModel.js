define([
    "libraries/jquery.cookie"
  ], function() {

    class NavControlsModel extends Backbone.Model {
      defaults() {
        return {
            "_cookieName":      "navControls",
            "_thumbsEnabled":   false,
            "_showAdapt":       true,
            "_showHyb":         true
        }
      }

      initialize() {
          this.listenTo(this, {
              "change": this.storeDataInCookie
          });
      }

      loadDataFromCookie() {
        let data = $.cookie(this.get("_cookieName"));
        if (!data) return;
        this.set(JSON.parse(data.replace(/'/g,'"')));
      }
      
      storeDataInCookie() {
        $.cookie(
            this.get("_cookieName"),
            JSON.stringify(this.toJSON()).replace(/"/g,"'"),
            {expires: 365}
        );
      }
    }

    class NavModel extends Backbone.Model {

        defaults() {
            return {
                _isActive: false
            }
        }

        initialize() {
            this.set("_controls", new NavControlsModel());
            this.getControls().loadDataFromCookie();
        }

        toJSON() {
            let json = super.toJSON();
            json._controls = json._controls.toJSON();
            return json;
        }

        getControls() {
            return this.get("_controls");
        }

        toggle() {
            this.set("_isActive", !this.get("_isActive"));
        }
    }

    return NavModel;
});