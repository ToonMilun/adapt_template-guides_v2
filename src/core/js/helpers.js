define([
    "dd",
    "text!content/guideTooltips.json"
], function (DD, GuideTooltipsData) {

    GuideTooltipsData = JSON.parse(GuideTooltipsData);

    var helpers = {

        ifeq: function(a, b, options) {
            if (a == b) {return options.fn(this);}
            return options.inverse(this);
        },

        ifnoteq: function(a, b, options) {
            if (a != b) {return options.fn(this);}
            return options.inverse(this);
        },
        
        /**
         * Allow JSON to be a template i.e. you can use handlebars {{expressions}} within your JSON
         * (Function copied from core Adapt helpers).
         */
        compile: function(template, context) {
            if (!template) {
            return '';
            }
            if (template instanceof Object) template = template.toString();
            var data = this;
            if (context) {
            // choose between a passed argument context or the default handlebars helper context
            data = (!context.data || !context.data.root ? context : context.data.root);
            }
            return Handlebars.compile(template)(data);
        },

        times: function(n, block) {
            var accum = '';
            for(var i = 0; i < n; ++i)
                accum += block.fn(i);
            return accum;
        },
    
        ifEquals: function(arg1, arg2, options) {
            return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
        },
    
        ifAny: function() {
    
            for (var i = 0; i < arguments.length-1; i++) {
                if (Boolean(arguments[i])) return arguments[arguments.length-1].fn(this);
            }
    
            return arguments[arguments.length-1].inverse(this);
        },
    
        ifNotEquals: function(arg1, arg2, options) {
            return (arg1 != arg2) ? options.fn(this) : options.inverse(this);
        },

        tooltip: function(tooltipName, context) {

            let name = tooltipName || context.hash.name || context.fn(this).toLowerCase();
            let content = context.fn(this) || name;
            
            let tooltip = GuideTooltipsData.global[name];
            if (!tooltip) {
                DD.log.error("{{#tooltip}}: no tooltip defined for: " + name);
                return "<span style='color: red'>" + content + "</span>";
            }

            return "<span class='term-tooltip' title='" + tooltip.body + "'>" + content + "</span>";
        },

        note: function(context) {
            let content = context.fn(this);
            return "<p class='note'><span style='font-weight: 500;'>Note:</span> " + content + "</p>";
        },

        error: function(context) {
            let content = context.fn(this);
            return "<p class='error'><span style='font-weight: bold; color: red; font-style: italic;'>" + content + "</span></p>";
        },

        tba: function(context) {
            let content = context.fn(this);
            return "<p class='tba'><span style='font-weight: 500;'>TBA:</span> " + content + "</p>";
        },

        customization: function(context) {
            let content = context.fn(this);
            return "<p class='customization'><span style='font-weight: 500;'>Customization:</span> " + content + "</p>";
        },

        example: function(context) {
            let content = context.fn(this);
            return "<p class='example'><span style='font-weight: 500;'>Example:</span> " + content + "</p>";
        },

        devlink: function(devPageName) {

            DD.log.error("{{#devlink}} helper not implemented.");
            return "<span style='color: red'>" + devPageName + "</span>";

            /*let targetPage = _.find(DD.pages.getPagesByType("dev"), (page) => {
                return page.get("_id").indexOf(devPageName) >= 0;
            });
            if (!targetPage) {
                DD.log.warn("Could not find devPage with name: " + devPageName);
                return "<strong style='color: red;'><em><u>MISSING LINK: " + devPageName + "</u></em></strong>";
            }
            return "<a href='#/dev/" + targetPage.get("_id") +"'>" + targetPage.get("name") + "</a>";*/
        },

        guidelink: function(pageName) {

            let targetPage = _.find(DD.pages.guides.getPagesByType("page"), (page) => {
                return page.get("_id") == pageName;
            });
            if (!targetPage) {
                DD.log.warn("Could not find page with name: " + pageName);
                return "<strong style='color: red;'><em><u>MISSING LINK: " + pageName + "</u></em></strong>";
            }
            return "<a href='#/guide/" + targetPage.get("_id") +"'>" + targetPage.get("name") + (targetPage.get("subName") ? (" (" + targetPage.get("subName") + ")") : "") + "</a>";
        },

        infolink: function(pageName) {

            return "<a href='#/info/" + pageName + "'>" + pageName + "</a>";
        },

        pagelink: function(pageName) {

            DD.log.error("{{#pagelink}} helper not implemented.");
            return "<span style='color: red'>" + pageName + "</span>";

            /*let targetPage = _.find(DD.pages.getPagesByType("page"), (page) => {
                return page.get("_id").indexOf(pageName) >= 0;
            });
            if (!targetPage) {
                DD.log.warn("Could not find page with name: " + pageName);
                return "<strong style='color: red;'><em><u>MISSING LINK: " + pageName + "</u></em></strong>";
            }
            return "<a href='#/guide/" + targetPage.get("_id") +"'>" + targetPage.get("name") + (targetPage.get("subName") ? (" (" + targetPage.get("subName") + ")") : "") + "</a>";*/
        },

        pluginExtraWarning: function(pluginName) {
            return Handlebars.compile(`<div class="pluginextrawarning"><p><strong>Warning: Plugin Extra!</strong><br/><br/>This functionality is <u><strong>NOT</strong></u> included in Adapt by default.<br/>To enable it, you must add <code>"<u>${pluginName || "ERROR"}</u>"</code> to your <code>project/config.json</code><br/>More info: {{{infolink "pluginExtra"}}}.</p></div>`)();
        },

        "hyb-grid": function(context) {
            
            let items = (context.fn ? context.fn(this) : "").split(",");
            let html = `<table class="hyb-grid">`;
            
            for (let i = 0; i < 3; i++) {

                html += `<tr>`

                for (let j = 0; j < 3; j++) {
                    
                    let item = items[i*3 + j];
                    let a = item.match(/\w/);
                    html += `<td class="hyb-grid__${a}">${item}</td>`;
                }

                html += `</tr>`
            }

            html += `</table>`;

            return Handlebars.compile(html)();
        }
    };

    // Register the helpers to the Handlebars object.
    for (var name in helpers) {
        if (helpers.hasOwnProperty(name)) {
             Handlebars.registerHelper(name, helpers[name]);
        }
    }

    return helpers;
});
