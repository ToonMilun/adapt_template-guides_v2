define([
    "dd",
    "core/js/views/pageView",
    "components/word/js/word" // FIXME: Needs to be loaded earlier to add its Handlebars helpers.
], function(DD, PageView) {

    /**
     * router.js
     * 
     * Monitors for changes to the URL
     * and serves as an entry point into the page.
     */

    // https://dzone.com/articles/backbone-tutorial-part-7
    
    class MainRouter extends Backbone.Router {

        initialize() {

            Backbone.history.start(); // Tells Backbone to start watching for hashchange events
        }

        /**
         * All Backbone Routes
         */
        routes() {
            return {
                /**
                 * When there is no hash on the url, the home method is called
                 */
                "": "index",

                "page/:pageName":   "handleRouteGuides", // Legacy
                
                "guide/:pageName":   "handleRouteGuides",
                "dev/:pageName":    "handleRouteDev",
                "dev":              "handleRouteDev",
                "tags/:tagNames":   "handleRouteTags",
                "info/:pageName":   "handleRouteInfo"
            }
        }

        renderGuidePage(pageName){

            // Remove previously rendered page.
            if (this.pageView) {
                this.pageView.remove();
            }

            // Render the corresponding page.
            this.pageView = new PageView({
                model: DD.pages.guides.getPageById(pageName)
            });
            DD.pages.guides.setPageActiveById(pageName);
            $(".main__inner").html(this.pageView.$el);
        }

        renderInfoPage(pageName) {

            require(["pages/info/js/infoModel", "pages/info/js/infoView"], (InfoModel, InfoView) => {
                
                let model = new InfoModel({
                    pageName: pageName
                });                
                let view = new InfoView({
                    model: model
                });

                // Remove previously rendered page.
                if (this.pageView) {
                    this.pageView.remove();
                }

                this.pageView = view;
                $(".main__inner").html(this.pageView.$el);
            });
        }

        renderTagsPage(tagNames) {

            let tags = tagNames.split(",");

            require(["pages/tags/js/tagsModel", "pages/tags/js/tagsView"], (TagsModel, TagsView) => {
                
                let tagsModel = new TagsModel();
                tagsModel.setActiveTagsByName(tags);

                let tagsView = new TagsView({
                    model: tagsModel
                });

                // Remove previously rendered page.
                if (this.pageView) {
                    this.pageView.remove();
                }

                this.pageView = tagsView;
                $(".main__inner").html(this.pageView.$el);
            });
        }

        renderDevPage(pageName){
            
            // Remove previously rendered page.
            if (this.pageView) {
                this.pageView.remove();
            }

            // If there's no pageName, show the devGuideHomePageView.
            if (!pageName) {
                require(["components/devGuide/js/devGuideHomePageView"], (DevGuideHomePageView) => {
                    this.pageView = new DevGuideHomePageView({
                        model: Core
                    });
                    $("#main").html(this.pageView.$el);
                });
            
            }
            // If there is a pageName, show the devGuidePageView
            else {
                require(["components/devGuide/js/devGuidePageView"], (DevGuidePageView) => {
                    this.pageView = new DevGuidePageView({
                        model: DD.pages.guides.getPageById(pageName)
                    });
                    $("#main").html(this.pageView.$el);
                });
            }
        }

        // Routes
        // ------------------------------------

        index() {
            //this.renderNav();
        }

        handleRouteTags(tagNames) {
            this.renderTagsPage(tagNames);
            //this.renderNav();
        }

        /**
         * 
         * @param {string} pageName     Name of the .json file being loaded from assets.
         */
        handleRouteGuides(pageName) {
            this.renderGuidePage(pageName);
            //this.renderNav();
        }

        /**
         * 
         * @param {string} pageName     Name of the .json file being loaded from assets.
         */
        handleRouteDev(pageName) {

            // If there's no pageName, show the devGuideMenu instead.
            this.renderDevPage(pageName);
            //this.renderNav();
        }

        /**
         * 
         * @param {string} pageName     Name of the .json file being loaded from assets.
         */
         handleRouteInfo(pageName) {

            // If there's no pageName, show the devGuideMenu instead.
            this.renderInfoPage(pageName);
            //this.renderNav();
        }
    }

    return MainRouter;
});