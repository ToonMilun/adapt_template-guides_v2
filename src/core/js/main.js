define([
    "backbone",
    'libraries/handlebars'
], function (Backbone, Handlebars) {

    // Global variables
    // ----------------
    // These are referred to constantly.
    window.Backbone = Backbone;
    window.Handlebars = Handlebars;

    class MainSingleton extends Backbone.Model {

        async initialize() {

            // Load the global object first.
            await this.loadDD();
            await this.loadLogging();

            // Load all Handlebars templates.
            this.DD.log.debug("Loading templates...");
            await this.initHandlebars();

            // Run start after the globals have loaded.
            require(["core/js/start"], (Start) => {
                let start = new Start();
            });
        }
        
        loadDD() {
            return new Promise(resolve => {
                require(["dd"], (DD) => {
                    this.DD = DD;
                    resolve();
                });
            });
        }

        loadLogging() {
            return new Promise(resolve => {
                require(["core/js/logging"], (DD) => {
                    resolve();
                });
            });
        }

        // Have all Handlebars templates be accessible from pageload.
        async initHandlebars() {
            
            return new Promise(resolve => {

                require(["text!templateData.json"], (SiteData) => {
                    
                    SiteData = JSON.parse(SiteData);
                    let count = 0;

                    Handlebars.templates = {};
                    _.each(SiteData, (path, name) => {

                        this.loadFile("text!" + path).then((result) => {
                            Handlebars.templates[name] = result;
                            count++;

                            if (count == Object.keys(SiteData).length) {
                                resolve();
                            }
                        });
                    });
                });
            });
        }

        loadFile(filename) {
            return new Promise(resolve => {
                require([filename], (file) => {
                    resolve(file);
                });
            });
        }
    }

    new MainSingleton();
});
