define([
    "dd",
    "text!content/tags.json",
    "core/js/models/tagModel",
    "core/js/views/tagView"
  ], function(DD, TagsJSON, TagModel, TagView) {

    let TagCollection = new Backbone.Collection(null, { comparator: 'name' });
    let Tags = {};

    Tags.collection = TagCollection;

    /**
     * Called by each PageModel to add itself to the TagModel's list of pages.
     * @param {*} tagName 
     * @param {*} pageModel 
     */
    Tags.registerPage = function(tagName, pageModel) {

        if (!pageModel) return;

        // Check if a tag with that name exists in the collection.
        let tagModel = TagCollection.findWhere({name: tagName});

        // If the tag doesn't exist yet, show a warning.
        if (!tagModel) {
            DD.log.warn("PageModel \"" + pageModel.get("type") + "\\" + pageModel.get("_id") + "\" has an undefined tag: \"" + tagName +"\"");
            return;
        }

        // Add the pageModel with this tag to the tags list of pageModels.
        tagModel.addPage(pageModel);
    };

    Tags.getTagByName = function(tagName){
        return _.findWhere(TagCollection, {name: tagName});
    }

    /*Tags.renderTagByName = function(tagName){
        return new TagView({
            model: Tags.getTagByName(tagName)
        }).$el;
    }*/

    Tags.renderTags = function(){
        return Tags.renderTagsByNames();
    }

    Tags.renderTagsByNames = function(tagNames){

        // Get all tags models with the provided tagNames.
        let models;
        
        if (tagNames !== undefined) {
            models = _.filter(Tags.collection.models, (model) => {
                return _.includes(tagNames, model.get("name"));
            });
        }
        // No filter. Render all tags.
        else {
            models = Tags.collection.models;
        }

        // Render each element.
        let $el = $();
        _.each(models, (model) => {
            $el = $el.add(new TagView({
                model: model
            }).$el);
        });

        return $el;
    }

    // Initialize all tags from JSON
    // ---------------------------------------------
    _.each(JSON.parse(TagsJSON), (tag) => {
        let tagModel = new TagModel(tag);
        TagCollection.add(tagModel);
    });

    DD.tags = Tags;
});