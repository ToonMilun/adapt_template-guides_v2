define([
  ], function() {

	/**
	 * Handles common / global variables.
	 * Can also be extended.
	 * 
	 * Will delay pageload until ready.
	 * 
	 * Based loosely on the adapt.js module in Adapt_v4.
	 */

	class DD extends Backbone.Model {
		
		async initialize() {

			await this.loadExtensions();
			console.log("Extensions loaded.");

			this.trigger("dd:start");
			this.setUpEventListeners();
		}

		setUpEventListeners() {
			$(window).resize(() => {
				this.trigger("device:resize", $(window).width(), $(window).height());
			});
		}

		/**
		 * Resolve once module has been loaded.
		 * @param {*} fileURL 
		 */
		awaitLoad(fileURL) {
			return new Promise(resolve => {
                require([fileURL], () => {
                    resolve();
                });
            });
		}

		loadExtensions() {
			
			return new Promise(resolve => {
        
				require(["text!extensionData.json"], (ExtensionData) => {

					ExtensionData = JSON.parse(ExtensionData);
					let count = 0;

					Handlebars.templates = {};
					_.each(ExtensionData, (path) => {

						require([path], (Extensions) => {
							count++;
							if (count == Object.keys(ExtensionData).length) {
								resolve();
							}
						});
					});
				});
			});
		}
	}

	let dd = new DD();
	dd.pages = {};
	return dd;
});