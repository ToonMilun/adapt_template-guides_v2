{
  "name": "Sticky",
  "subName": "",
  "type": "template",
  "tags": [
    "text-small",
    "text-medium",
    "text-large",
    "image-large",
    "items",
	"sequential"
  ],
  "components": {

    "preview": {
		"_examples": [
			{
				"title": "SET-RW-23",
				"_url": "sticky-1"
			}
		]
	},

    "description": {
		"body": <<<"

			{{#error}}This template is complete, but currently doesn't match the style of all the others.{{/error}}

			<p>The Sticky template is made up of two columns. One column contains each reveal's text, the other, contains each reveal's {{#tooltip "graphic"}}graphic{{/tooltip}}.</p>

			<p>This template is for displaying sequential information / a sequence of events. This is one of the few templates that supports the {{{guidelink "stickyElement"}}} extra, and of all templates it utilizes it the best.</p>

			<h3>The text column</h3>

			<p>This is the column that contains the {{#tooltip "standup"}}standup{{/tooltip}} text, followed by each reveal's text in order.</p>

			<p>As the user scrolls the main page, this text will scroll too, meaning that the user literally scrolls through each of the reveals.</p>


			<h3>The graphic column</h3>

			<p>The other column in the template contains each reveal's graphic. Unlike the scrolling text column, the graphic column will "sticky" to the screen once the user scrolls it into view. It will then change its graphic based on what standup/reveal is currently scrolled into view in the text column.</p>

			<p>The graphics will crossfade between each other as the reveals change by defautlt. However, you can choose to have custom transitions, or even have the same graphic "build itself up" across reveals by using a {{{guidelink "stickyElement"}}}.</p>
		">>>,
		"_examples": [
			<<<"
				Showing the user a set of steps with decorative images for each standup + reveal.
			">>>,
			<<<"
				Showing the user a set of steps, and have a {{{guidelink "stickyElement"}}} modify itself as the user scrolls.
				{{#example}}Standup - Has an empty table. R1 - The first row of the table gets filled out. R2 - The second row of the table gets filled out. R3 - etc.{{/example}}
				{{#example}}Standup - Show a messy warehouse. R1 - "Start by removing the large boxes" (update the image to show the boxes removed). R2 - "Sweep up any broken glass" (update the image to remove the broken glass). R3 - etc.{{/example}}
			">>>
		]
	},
	"modularity": {
		"body": <<<"
			<p>This template supports the following modules:</p>
		">>>,
		"_parents": {
			"_includeTags": [],
			"_excludeTags": [],
			"_includePages": [],
			"_excludePages": []
		}
	},
    "word": {
      "_items": [
        {
          "_header": {
            "_isEnabled": 	true,
            "tn": 			"",
            "notes": 		""
          },
  
          "_comment": 	"",
          "title":		"Title__StickyWithImages",
          "body":			"{{#id-standup-text}}{{/id-standup-text}}",
          "instruction":	"",
          "content":		"",
  
          "_items": [
            {
              "_comment": "",
              "_header":	"{{#id-item-header}}1{{/id-item-header}}",
              "title": 	"",
              "body": 	"{{#id-reveal-text}}{{/id-reveal-text}}",
              "instruction": 	"",
              "content": ""
            },
            {
              "_comment": "",
              "_header":	"{{#id-item-header}}2{{/id-item-header}}",
              "title": 	"",
              "body": 	"{{#id-reveal-text}}{{/id-reveal-text}}",
              "instruction": 	"",
              "content": ""
            }
          ]
        },
        {
          "_header": {
            "_isEnabled": 	true,
            "tn": 			"",
            "notes": 		""
          },
  
          "_comment": 	"",
          "title":		"Title__StickyWithCustomsAsGraphics",
          "body":			"{{#id-standup-text}}{{/id-standup-text}}",
          "instruction":	"",
          "content":		"",
  
          "_items": [
            {
              "_comment": "Table as a graphic",
              "_header":	"R1][Use the table for the graphic",
              "title": 	"",
              "body": 	"{{#id-reveal-text}}{{/id-reveal-text}}<br/><br/><table><tr><td>Example table</td><td>Example table</td><td>Example table</td></tr><tr><td>Example table</td><td>Example table</td><td>Example table</td></tr><tr><td>Example table</td><td>Example table</td><td>Example table</td></tr></table>",
              "instruction": 	"",
              "content": ""
            },
            {
              "_comment": "Video as a graphic",
              "_header":	"R2][Use provided video for the graphic",
              "title": 	"",
              "body": 	"{{#id-reveal-text}}{{/id-reveal-text}}<br/><br/>{{#w-yellow}}Transcript][{{#id-video}}{{/id-video}}{{/w-yellow}}<br/><br/>TranscriptTextTranscriptTextTranscriptTextTranscriptTextTranscriptTextTranscriptTextTranscriptTextTranscriptTextTranscriptTextTranscriptTextTranscriptTextTranscriptTextTranscriptTextTranscriptText",
              "instruction": 	"",
              "content": ""
            }
          ]
        }
      ]
    },
    "developer": {
      "body": <<<"
	  		<p>This template is quite unique, so please read this description carefully.</p>
		  
		  	<p>Much like the {{#pagelink "static-multi"}}{{/pagelink}}, this template is made by combining many <code>text</code> and <code>graphic</code> components side-by-side.</p>
		  
		  	<p>The major difference is, the Sticky requires its parent <strong>block</strong> to have unique settings, not its components. I have provided an example of this below.</p>
		  
		  	<p><strong>Note:</strong> the Sticky will work with <em>any type</em> of <code>&quot;left&quot;</code> + <code>&quot;right&quot;</code> compatible components placed inside it.</p>
		  
			<br/>

			<h3>Component <code><strong>_layout</strong></code> matters!</h3>

		  	<p>In the example below, you will notice that the order the components are in are:</p>
			
			<p>
			<code>right</code><br/>
			<code>left</code><br/>
			<code>right</code><br/>
			<code>left</code>
			</p>
			
			<p>There are two reasons for this:</p>
			<ul>
				<li>If the user views this template on a phone, the ordering used above will make the graphic components appear before the text components that they're paired with (this is an intentional design choice).</li>
				<li>The script which makes this template needs <strong>all scrolling components to be <code>left</code>, and all sticky components to be <code>right</code></strong>. If you want to swap the two columns, use the Block's <code>_stickyBlock._layout</code> property below.<br/><br/>If you're confused; just follow the Component(s) example below and don't change any <code>_layout</code> properties.</li>
			</ul>
	">>>,
      "json": {
        "blocks": [
          {
            "_desc": "Use this to define a sticky <u>block</u>.",
            "_items": [
              {
                "_id": "b-X",
                "_parentId": "a-X",
                "_classes": "",
                "_stickyBlock": {
                  "_isEnabled": true,
                  "_layout": "left"
                },
                "_sizing": {
                  "_width": 4,
                  "_maxWidth": true
                },
                "<<<~": "",
                "_trackingId": 0,
                "_type": "block",
                "~>>>": ""
              }
            ],
            "properties": {
              "_stickyBlock": {
                "_desc": "This property sets up the block to become a sticky block (is enabled)."
              },
              "_layout": {
                "_desc": "Which side should the <strong>scrolling</strong> column (the column with text in it) sit on.",
                "_params": "left | right"
              },
              "_componentWidth": {
                "_desc": "The width of the <strong>scrolling</strong> column on large devices.<br/>6 = 50%, 9 = 75%, 12 = 100%, etc.",
                "_params": "1 - 12"
              },
              "_sizing": {
                "_isOptional": false,
                "_desc": "Used to set the widths of the <strong>sticky</strong> column's components."
              },
              "_sizing._width": {
                "_isOptional": false,
                "_desc": "Width of the sticky column.",
                "_comment": "inherit"
              },
              "_sizing._maxWidth": {
                "_desc": "Max width of the sticky column on smaller devices.",
                "_comment": "inherit"
              }
            }
          }
        ],
        "components": [
          {
            "_desc": <<<"
				Text + images (most common usage example).
			">>>,
            "_items": [
              {
                "_id": "",
                "_parentId": "b-X",
                "_component": "graphic",
                "_layout": "right",
                "_classes": "",
                "_sizing": {
                  "_maxWidth": ""
                },
                "body": "",
                "_graphic": {
                  "large": "course/en/images/b-X/pic.jpg",
                  "small": "course/en/images/b-X/pic.jpg",
                  "style": "",
                  "alt": "",
                  "attribution": ""
                },
				"footer": "<<<>>>",
                "<<<~": "",
                "_isOptional": true,
                "_type": "component",
                "~>>>": ""
              },
              {
                "_id": "",
                "_parentId": "b-X",
                "_component": "text",
                "_layout": "left",
                "_classes": "",
                "title": "XXX",
                "body": "<<<XXX>>>",
                "<<<~": "",
                "_type": "component",
                "~>>>": ""
              },
              {
                "_id": "",
                "_parentId": "b-X",
                "_component": "graphic",
                "_layout": "right",
                "_classes": "",
                "_sizing": {
                  "_maxWidth": ""
                },
                "body": "",
                "_graphic": {
                  "large": "course/en/images/b-X/pic.jpg",
                  "small": "course/en/images/b-X/pic.jpg",
                  "style": "",
                  "alt": "",
                  "attribution": ""
                },
				"footer": "<<<>>>",
                "<<<~": "",
                "_isOptional": true,
                "_type": "component",
                "~>>>": ""
              },
              {
                "_id": "",
                "_parentId": "b-X",
                "_component": "text",
                "_layout": "left",
                "_classes": "",
                "title": "XXX",
                "body": "<<<XXX>>>",
                "<<<~": "",
                "_type": "component",
                "~>>>": ""
              }
            ],
			"properties": {
              "_sizing": {
                "_desc": "<p>The sizing for the components is defined in the <strong>block</strong>, however, if you wish to have a custom <code>_maxWidth</code> for a specific component(s), you can explicitly set it here.</p>"
              }
            }
          },
		  {
            "_desc": <<<"
				Text + graphics (tables/animations/custom HTML/etc).
			">>>,
            "_items": [
              {
                "_id": "",
                "_parentId": "b-X",
                "_component": "graphic",
                "_layout": "right",
                "_classes": "",
                "_sizing": {
                  "_maxWidth": ""
                },
                "body": "<<<PLACE_CUSTOM_HTML_HERE>>>",
				"footer": "<<<>>>",
                "<<<~": "",
                "_isOptional": true,
                "_type": "component",
                "~>>>": ""
              },
              {
                "_id": "",
                "_parentId": "b-X",
                "_component": "text",
                "_layout": "left",
                "_classes": "",
                "title": "XXX",
                "body": "<<<XXX>>>",
                "<<<~": "",
                "_type": "component",
                "~>>>": ""
              },
              {
                "_id": "",
                "_parentId": "b-X",
                "_component": "graphic",
                "_layout": "right",
                "_classes": "",
                "_sizing": {
                  "_maxWidth": ""
                },
                "body": "<<<PLACE_CUSTOM_HTML_HERE>>>",
				"footer": "<<<>>>",
                "<<<~": "",
                "_isOptional": true,
                "_type": "component",
                "~>>>": ""
              },
              {
                "_id": "",
                "_parentId": "b-X",
                "_component": "text",
                "_layout": "left",
                "_classes": "",
                "title": "XXX",
                "body": "<<<XXX>>>",
                "<<<~": "",
                "_type": "component",
                "~>>>": ""
              }
            ]
          }
        ],
		"html": [
			{
				"_desc": "Stickys with <em>sticky</em> graphics",
				"_example": <<<"
					<p>The Sticky template is one of the few templates which supports the {{{guidelink "stickyElement"}}} Extra; which allows the Sticky template to "hold" a graphic between reveals while having it transform itself.</p>

					<p>There's actually <em>two</em> different ways to get the Sticky Element morphing working in this template. Both are explained here: {{{guidelink "stickyBlockSticky"}}}.</p> 
				">>>
			}
		]
      }
    }
  }
}