{
	"name": "01 - HYB Tables - Intro",
	"subName": "",
	"type": "template",
	"tags": [
		"table"
	],
	"components": {

		"fiddle": {
			"body": <<<"
				<p>This collection of guides will cover the following:</p>

				<ul>
					<li>How to design different types of tables in the ID doc.</li>
					<li>How to troubleshoot tables which are too big / complicated to fit inside of a Hybrid screen.</li>
					<li>Different visual themes you can apply to tables based on context.</li>
				</ul>

				<p>These guides are intended for both IDers <em>and</em> developers, and will provide examples for both.</p>

				<p>If you have any further queries / suggestions for guides, please let Milton Plotkin know so that he can add them.</p>

				<p><strong>Developers</strong>, be sure to check the developer section of each guide for further instructions.</p>
			">>>,
			"_items": []
		},

		"developer": {
			"body": <<<"
				<p>Tables in Hybrid/Adapt are (mostly) made using the normal HTML table tags and rules.</p>

				<hr>

				<h3>Understanding the basics of HTML tables</h3>

				<p>To familiarize yourself with HTML tables, please read through the following tutorials from W3Schools (they will cover the basics).</p>

				<ul>
					<li><a target="_blank" href="https://www.w3schools.com/html/html_tables.asp">Intro to tables</a></li>
					<li><a target="_blank" href="https://www.w3schools.com/html/html_table_headers.asp">Table headers</a></li>
					<li><a target="_blank" href="https://www.w3schools.com/html/html_table_colspan_rowspan.asp">Table colspan & rowspan</a> (merging table cells)</li>
				</ul>


				<hr>

				
				<h3>Complex tables need custom LESSCSS</h3>

				<p>The larger a table is, the more likely you will need to give it <strong>custom LESSCSS styles</strong>. These can be used to:</p>

				<ul>
					<li>Set the text alignment inside of cells.</li>
					<li>Set table colour themes.</li>
					<li>Set table widths.</li>
					<li>A lot of other things (not covered).</li>
				</ul>

				<p>To add custom LESSCSS to your table, do the following:</p>

				<h4>Step 1 - Give your table a class</h4>

				<p>Give your <code>&lt;table&gt;</code> element a <strong>custom</strong> class name.</p>
				
				<p>Custom classes <strong>must</strong> start with a '<code>c-</code>' prefix.</p>

				<p>For example:</p>

				{{#pre}}
<table <mark>class="<mark>c-example</mark>"</mark> >
	<tr>
		<th>111</th>
		<th>222</th>
		<th>333</th>
	</tr>
	<tr>
		<td>AAA</td>
		<td>BBB</td>
		<td>CCC</td>
	</tr>
	<tr>
		<td>DDD</td>
		<td>EEE</td>
		<td>FFF</td>
	</tr>
</table>
				{{/pre}}


				<h4>Step 2 - Open <code>customs.less</code></h4>

				<p>Find and open the <code>customs.less</code> file. This is located in <code>src/course/en/project/less/custom.less</code></p>


				<h4>Step 3 - Create a parent selector for your table.</h4>

				<p>In the <code>customs.less</code>, add a LESSCSS selector for for your table.</p>

				<p>A 'selector' is how you tell LESSCSS which HTML element(s) you want certain rules to apply to. In the following example, we make it apply ONLY to table(s) with the <code>c-example</code> class.</p>
				
				<p>For example:</p>

				{{#pre}}
<mark>.c-example</mark> {
	
	
}
				{{/pre}}

				<p>This selector won't do anything yet; but no matter what rules you add to it, you can be sure it will <strong>only</strong> affect table(s) with the <code>c-example</code> class.</p>

				<p>Don't forget to add the <code>.</code> before the classname!</p>


				<h4>Step 4 - Decide what parts of the table you want to style.</h4>

				<p>Think about what parts of the table you want to add custom styles to. The first column? Everything <em>but</em> the first column? The second row?</p>
				
				<p>There's many different ways to tell LESSCSS what to affect (which I can't cover); but here's some examples to get you started.</p>

				{{#pre}}
.c-example {
	
	// Affect the ENTIRE table
	& {

	}

	// Affect ALL normal cells that aren't headers
	td {

	}

	// Affect only the cells in the third column, and only the normal cells
	td:nth-child(3) {

	}

	// Affect the cells in second (2), third, fourth, etc. columns, and only the normal cells
	td:nth-child(n+2) {

	}

	// Affect the normal cells in the second row.
	tr:nth-child(2) td {

	}
}
				{{/pre}}

				
				<h4>Step 5 - Apply your style rules.</h4>

				<p>Now, specify what you want to happen to the elements you've selected using CSS style rules.</p>

				<p>For example, the following will make all the normal cells in column right align; and all the cells in column 3 italic.</p>

				{{#pre}}
.c-example {
	
	td:nth-child(2) {
		text-align: right;
	}

	td:nth-child(3) {
		font-style: italic;
	}
}
				{{/pre}}

				<p>There are <strong>a lot</strong> of CSS style rules available; but don't worry. For Hybrid tables, you will only need to use a <strong>few</strong>.</p>


				<br/>
				<h3>What CSS styles do I need to know?</h3>
				
				<p>You only need to use the styles which Hybrid cannot auto-determine for you.</p>

				<p>Here's a list of styles you can use.<br/><strong>Unless your name is Jacob or Milton, please do not use other styles!</strong></p>

				<ul>
					<li><code><a href="https://www.w3schools.com/cssref/pr_text_text-align.php">text-align</a></code> (align text horizontally)</li>
					<li><code><a href="https://www.w3schools.com/cssref/pr_font_weight.php">font-weight</a></code> (set text to bold)</li>
					<li><code><a href="https://www.w3schools.com/csSref/pr_font_font-style.php">font-style</a></code> (set text to italic)</li>
					<li><code>width</code> (covered in one of our guides)</li>
				</ul>


				<hr>

				
				<h3>Mixins</h3>

				<p>In the example LESSCSS, you will come across <strong>LESSCSS Mixins</strong> (they end in <code>()</code>):</p>

				{{#pre}}
.c-example {
	
	td:nth-child(1) {
		<mark>.table-theme-subheader();</mark> // Tells the table cell(s) selected to use "theme-subheader".
	}

	td:nth-child(2) {
		<mark>.table-theme-label();</mark> // Tells the table cell(s) selected to use "theme-label".
	}
}
				{{/pre}}

				<p>These are basically multiple CSS properties combined into one.</p>


				<hr>

				
				<h3>Anything else?</h3>

				<p>No that's about it. You can start reading the other guides now.</p>
		  	">>>,
			"json": {
				"html": [],
				"blocks": [],
				"components": [],
				"properties": {}
			}
      	}
	}
}