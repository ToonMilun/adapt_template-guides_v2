{
	"name": "08 - HYB Tables - Themes / appearance",
	"subName": "",
	"type": "template",
	"tags": [
		"table"
	],
	"components": {

		"fiddle": {
			"body": <<<"
				
				<p>There are many ways to style the appearance of a table in Hybrid.</p>	

				<p>These are outlined below. Note that (for consistency), please only use non-<strong>default</strong> themes when necessary.</p>
			">>>,
			"_items": [
				{
					"title": "Default theme",
					"body": <<<"
						<p>If no theme is specified, the table will use a generic blue appearance.</p>
					">>>,
					"_word": {
						"body": <<<"
							<table>
								<tr>
									<th>Fruits</th>
									<th>Vegetables</th>
								</tr>
								<tr>
									<td>Apple</td>
									<td>Artichoke</td>
								</tr>
								<tr>
									<td>Banana</td>
									<td>Broccoli</td>
								</tr>
								<tr>
									<td>Cherry</td>
									<td>Carrot</td>
								</tr>
							</table>
						">>>
					},
					"_html": {
						"body": <<<P
<table>
	<tr>
		<th>Fruits</th>
		<th>Vegetables</th>
	</tr>
	<tr>
		<td>Apple</td>
		<td>Artichoke</td>
	</tr>
	<tr>
		<td>Banana</td>
		<td>Broccoli</td>
	</tr>
	<tr>
		<td>Cherry</td>
		<td>Carrot</td>
	</tr>
</table>
						P>>>,
						"less": <<<P
						P>>>
					},
					"_output": {
						"body": <<<"
						">>>
					}
				},
				<<<! ---------------------------------------------------------------------------------------------------- !>>>
				{
					"title": "Stripes",
					"body": <<<"
						<p>A 'striped' table has its rows / groups alternate their colours to improve readability. You can add stripes to any table, and most table themes support them.</p>

						<p>Use stripes when you have a lot of rows / wherever you deem them appropriate. Note that if stripes are used, you <strong>cannot</strong> also use label cells ({{{guidelink "hyb-02-cells"}}}).</p>
					">>>,
					"_word": {
						"body": <<<"
							{{#w-yellow}}Use stripes{{/w-yellow}}
							<table class="theme-stripe">
								<tr>
									<th>Fruits</th>
									<th>Vegetables</th>
								</tr>
								<tr>
									<td>Apple</td>
									<td>Artichoke</td>
								</tr>
								<tr>
									<td>Banana</td>
									<td>Broccoli</td>
								</tr>
								<tr>
									<td>Cherry</td>
									<td>Carrot</td>
								</tr>
								<tr>
									<td>Durian</td>
									<td>Dill</td>
								</tr>
								<tr>
									<td>Eggplant?</td>
									<td>Eggplant?</td>
								</tr>
							</table>
						">>>,
						"comments": [
							"You need to explicitly state that stripes are being used so the dev doesn't confuse them for 'label cells'."
						]
					},
					"_html": {
						"body": <<<P
<table class="theme-stripe">
	<tr>
		<th>Fruits</th>
		<th>Vegetables</th>
	</tr>
	<tr>
		<td>Apple</td>
		<td>Artichoke</td>
	</tr>
	<tr>
		<td>Banana</td>
		<td>Broccoli</td>
	</tr>
	<tr>
		<td>Cherry</td>
		<td>Carrot</td>
	</tr>
	<tr>
		<td>Durian</td>
		<td>Dill</td>
	</tr>
	<tr>
		<td>Eggplant?</td>
		<td>Eggplant?</td>
	</tr>
</table>
						P>>>,
						"less": <<<P
						P>>>
					},
					"_output": {
						"body": <<<"
						">>>
					}
				},
				<<<! ---------------------------------------------------------------------------------------------------- !>>>
				{
					"title": "Print(out) theme",
					"body": <<<"
						<p>If you have a printout/form/paper in the content, and it has a table, this theme should be used. It will make the table greyscale, as if it was printed using a printer with no colour ink (similar to how most real forms would have it).</p>

						<p><strong>Note:</strong> theme may change appearance slightly in future.</p>
					">>>,
					"_word": {
						"body": <<<"
							{{#w-yellow}}Use PRINT theme{{/w-yellow}}
							<table>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>D.O.B</th>
									<th>Signature</th>
								</tr>
								<tr>
									<th>1</th>
									<td style="font-family: cursive;">Castle cat</td>
									<td style="font-family: cursive;">2004</td>
									<td>{{#w-yellow}}Signature{{/w-yellow}}</td>
								</tr>
								<tr>
									<th>2</th>
									<td style="font-family: cursive;">Dungeon dog</td>
									<td style="font-family: cursive;">2007</td>
									<td>{{#w-yellow}}Signature{{/w-yellow}}</td>
								</tr>
							</table>
						">>>,
						"comments": [
							"Themes are specified in all-caps (makes them immediately obvious).",
							"Note that, despite the output having a vastly different colourscheme, the colour-coding of cells in the ID doc remains <strong>exactly the same</strong>",
							"The use of Comic Sans in this example communicates to the devs: 'make this text appear handwritten'."
						]
					},
					"_html": {
						"body": <<<P
<table class="theme-print">
	<tr>
		<th>#</th>
		<th>Name</th>
		<th>D.O.B</th>
		<th>Signature</th>
	</tr>
	<tr>
		<th>1</th>
		<td class="u-font-cursive">Castle cat</td>
		<td class="u-font-cursive">2004</td>
		<td class="u-font-cursive">♠♣♥♦</td>
	</tr>
	<tr>
		<th>2</th>
		<td class="u-font-cursive">Dungeon dog</td>
		<td class="u-font-cursive">2007</td>
		<td class="u-font-cursive">🂡🂿🃉</td>
	</tr>
</table>
						P>>>,
						"less": <<<P

						P>>>
					},
					"_output": {
						"body": <<<"
						">>>
					}
				},
				<<<! ---------------------------------------------------------------------------------------------------- !>>>
				{
					"title": "Base/basic theme",
					"body": <<<"
						<p>The blandest table theme. Returns the table to a default HTML style appearance.</p>

						<p>Very rarely used. Good for when you want to reduce visual/colour noise of a screen without using the print theme.</p>
					">>>,
					"_word": {
						"body": <<<"
							{{#w-yellow}}Use BASE theme{{/w-yellow}}
							<table>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>D.O.B</th>
									<th>Signature</th>
								</tr>
								<tr>
									<th>1</th>
									<td style="font-family: cursive;">Castle cat</td>
									<td style="font-family: cursive;">2004</td>
									<td>{{#w-yellow}}Signature{{/w-yellow}}</td>
								</tr>
								<tr>
									<th>2</th>
									<td style="font-family: cursive;">Dungeon dog</td>
									<td style="font-family: cursive;">2007</td>
									<td>{{#w-yellow}}Signature{{/w-yellow}}</td>
								</tr>
							</table>
						">>>
					},
					"_html": {
						"body": <<<P
<table class="theme-base">
	<tr>
		<th>#</th>
		<th>Name</th>
		<th>D.O.B</th>
		<th>Signature</th>
	</tr>
	<tr>
		<th>1</th>
		<td class="u-font-cursive">Castle cat</td>
		<td class="u-font-cursive">2004</td>
		<td class="u-font-cursive">♠♣♥♦</td>
	</tr>
	<tr>
		<th>2</th>
		<td class="u-font-cursive">Dungeon dog</td>
		<td class="u-font-cursive">2007</td>
		<td class="u-font-cursive">🂡🂿🃉</td>
	</tr>
</table>
						P>>>,
						"less": <<<P

						P>>>
					},
					"_output": {
						"body": <<<"
						">>>
					}
				},
				<<<! ---------------------------------------------------------------------------------------------------- !>>>
				{
					"title": "Ghost theme",
					"body": <<<"
						<p>This theme makes the table borders completely invisible.</p>

						<p>This is very rarely used, and mainly by the devs for customs. You shouldn't need to specify it.</p>
					">>>,
					"_word": {
						"body": <<<"
							{{#w-yellow}}Overlay this table over the decorative table image.{{/w-yellow}}
							<table>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>D.O.B</th>
									<th>Signature</th>
								</tr>
								<tr>
									<th>1</th>
									<td style="font-family: cursive;">Castle cat</td>
									<td style="font-family: cursive;">2004</td>
									<td>{{#w-yellow}}Signature{{/w-yellow}}</td>
								</tr>
								<tr>
									<th>2</th>
									<td style="font-family: cursive;">Dungeon dog</td>
									<td style="font-family: cursive;">2007</td>
									<td>{{#w-yellow}}Signature{{/w-yellow}}</td>
								</tr>
							</table>
						">>>,
						"customs": [
							"The yellow text given here is just for the sake of example."
						]
					},
					"_html": {
						"body": <<<P
<table class="theme-ghost">
	<tr>
		<th>#</th>
		<th>Name</th>
		<th>D.O.B</th>
		<th>Signature</th>
	</tr>
	<tr>
		<th>1</th>
		<td class="u-font-cursive">Castle cat</td>
		<td class="u-font-cursive">2004</td>
		<td class="u-font-cursive">♠♣♥♦</td>
	</tr>
	<tr>
		<th>2</th>
		<td class="u-font-cursive">Dungeon dog</td>
		<td class="u-font-cursive">2007</td>
		<td class="u-font-cursive">🂡🂿🃉</td>
	</tr>
</table>
						P>>>,
						"less": <<<P
.theme-ghost {
	th {
		color: black !important;
	}
}
						P>>>
					},
					"_output": {
						"body": <<<"
						">>>
					}
				},
				<<<! ---------------------------------------------------------------------------------------------------- !>>>
				{
					"title": "Custom theme",
					"body": <<<"
						<p>If you need a custom colourscheme for a table, there's two ways to ask for it:</p>

						<ul>
							<li>Describe (with text) what colourscheme to use. For example: "Give this table a red colourscheme".</li>
							<li>Colour the table yourself (in the ID doc), and have the devs copy your style. See example below.</li>
					">>>,
					"_word": {
						"body": <<<"
							{{#w-yellow}}Use the following colourscheme.{{/w-yellow}}
							<table>
								<tr>
									<th style="background: #c00000; color: white;">#</th>
									<th style="background: #c00000; color: white;">Name</th>
									<th style="background: #c00000; color: white;">D.O.B</th>
									<th style="background: #c00000; color: white;">Signature</th>
								</tr>
								<tr>
									<th style="background: #c00000; color: white;">1</th>
									<td style="font-family: cursive; background: #ebb0b0;">Castle cat</td>
									<td style="font-family: cursive; background: #ebb0b0;">2004</td>
									<td style="background: #ebb0b0;">{{#w-yellow}}Signature{{/w-yellow}}</td>
								</tr>
								<tr>
									<th style="background: #c00000; color: white;">2</th>
									<td style="font-family: cursive; background: #ebb0b0;">Dungeon dog</td>
									<td style="font-family: cursive; background: #ebb0b0;">2007</td>
									<td style="background: #ebb0b0;">{{#w-yellow}}Signature{{/w-yellow}}</td>
								</tr>
							</table>
						">>>,
						"customs": [
							"If you use a custom colourscheme, the devs will no longer be able to tell the types of the cells. As such, the colourscheme should be 'obvious enough' for them to intuit which cells are which.",
							"Your colourscheme doesn't have to be perfect. Devs will tweak the colourscheme if it clashes too much with itself/surroundings."
						]
					},
					"_html": {
						"body": <<<P
<table class="c-theme-red">
	<tr>
		<th>#</th>
		<th>Name</th>
		<th>D.O.B</th>
		<th>Signature</th>
	</tr>
	<tr>
		<th>1</th>
		<td class="u-font-cursive">Castle cat</td>
		<td class="u-font-cursive">2004</td>
		<td class="u-font-cursive">♠♣♥♦</td>
	</tr>
	<tr>
		<th>2</th>
		<td class="u-font-cursive">Dungeon dog</td>
		<td class="u-font-cursive">2007</td>
		<td class="u-font-cursive">🂡🂿🃉</td>
	</tr>
</table>
						P>>>,
						"less": <<<P
.c-theme-red {
	th {
		background: #c00000;
		color: white;
	}

	td {
		background: #ebb0b0;
	}
}
						P>>>
					},
					"_output": {
						"body": <<<"
						">>>
					}
				}
			]
		},



		"developer": {
			"body": <<<"

				<p>Applying (existing) table themes is quite simple. Simply add one (or more) of the following classes to the <code>&lt;table&gt;</code>.</p>

				<ul>
					<li><code>theme-print</code></li>
					<li><code>theme-stripe</code></li>
					<li><code>theme-base</code></li>
					<li><code>theme-ghost</code></li>
				</ul>

				<p>For custom themes, you will need to do a bit of <code>custom.less</code> (if the table theme is extremely complex, feel free to contact Jacob or Milton to assist you).</p>
		  	">>>,
			"json": {
				"html": [
					{
						"_desc": "Custom table theme.",
						"_example": <<<"

							<p>The following table will use a red colourscheme.</p>
{{#pre}}
<table <mark>class="c-theme-red"</mark> >
	<tr>
		<th>Fruits</th>
		<th>Vegetables</th>
	</tr>
	<tr>
		<td>Apple</td>
		<td>Artichoke</td>
	</tr>
	<tr>
		<td>Banana</td>
		<td>Broccoli</td>
	</tr>
	<tr>
		<td>Cherry</td>
		<td>Carrot</td>
	</tr>
</table>
{{/pre}}
							<p>And now, change the table colours using <code>custom.less</code>:</p>
							
{{#pre}}
<mark>.c-theme-red</mark> {

	// Make all <th> elements a dark red.
	th {
		background: #c00000;
		color: white;
	}

	// Make all <td> elements light red.
	td {
		background: #ebb0b0;
	}
}
{{/pre}}

						">>>
					}
				],
				"blocks": [],
				"components": [],
				"properties": {}
			}
      	}
	}
}