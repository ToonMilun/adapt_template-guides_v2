{
  "name":    "Figure containers",
  "subName": "",
  "type": "extra",
  "tags": [
    "custom",
    "text-small",
    "image-small",
    "image-large",
    "extra-unrestricted"
  ],
  "components": {

    "preview": {
      "layouts": [
        {
          "title":      "",
          "hasLabels":  false
        }
      ]
    },

    "description": {
      "body": "Used to layout extra content."
    },

    "modularity": {
      "_parents": {
        "_includeTags": ["all"],
        "_excludeTags": ["extra-unrestricted", "extra-restricted"],
        "_includePages": ["extraLearning", "resourcePopup", "stickyElement"],
        "_excludePages": ["hotgraphic-mini", "media"]
      }
    },

    "word": {
      "_items": [
        {
          "_header": {
            "_isEnabled": 	true,
            "tn": 			"{{#w-debug}}Insert template compatible with this extra here{{/w-debug}}",
            "notes": 		""
          },
  
          "_comment": 	"A \"figure container\" is something added by the developers to hold specific kinds of custom inline content (most often, images, tables, codeboxes, etc). You do not have to specify them in the ID. Simply insert them on a new line, and the developers will handle the rest.<br/><br/>Below are some examples of the kind of content that the developers will wrap in a figure container.",
          
          "title":		"Title",
          "body":			"<table><tr><td>Example table</td><td>Example table</td><td>Example table</td></tr><tr><td>Example table</td><td>Example table</td><td>Example table</td></tr><tr><td>Example table</td><td>Example table</td><td>Example table</td></tr></table><br/>{{#id-standup-text basic=true}}{{/id-standup-text}}",
          "instruction":	"",
          "content":		"",

          "_items": [
            {
              "_comment": "Decorative text figure example.",
              "_header":	"{{#id-item-header}}1{{/id-item-header}}",
              "_type": "",
              "title": 	"",
              "body": 	"Here is an example of how you would provide the user with a paragraph of text, and then follow it up with some large, isolated (almost \"image\" like) text.<figure><span style='font-size: 3rem;'>E = MC<sup>2</sup></span></figure>You can then add more text underneath it and just keep on going. Content like this is considered a \"figure\" by the developers.",
              "instruction": 	"",
              "content": ""
            }
          ]
        }
      ]
    },

    "developer": {
			"body": <<<"
				<p>The figure helpers are used to create custom layouts for other Extras within existing templates.</p>
				
				<p>Here are some examples where the figure helpers can be used:</p>

				<ul>
					<li>You need to insert an additional graphic(s) (image/table/animation/etc) into your template.</li>
					<li>You want to do the above, but need two or more graphics side-by-side.</li>
					<li>If you need to display <code>elementB</code> on the left, and <code>elementA</code> on the right on a large device, but then have them display as <code>elementA</code> followed by <code>elementB</code> on a small device.</li>
					<<<!<li>Any of the above + a caption underneath.</li>!>>>
				</ul>
				
				<p><strong>Tip:</strong> Every other Extra which requires the <code>\\{{#figure}}</code> helper will include it in its examples, so you don't need to worry about memorising the content below.</p>
			">>>,
			"json": {
				"helpers": [
				{
					"_desc": "An single graphic between two paragraphs.",
					"_items": [
						{
							"_name": "figure",
							"_content": "PLACE_ITEM_HELPERS_IN_HERE",
							"_properties": {
							}
						},
						{
							"_name": "item",
							"_content": "PLACE_YOUR_CUSTOM_CONTENT_IN_HERE",
							"_properties": {
								"sizing": "4"
							}
						}
					],
					"properties": {
						"sizing": {
							
						},
						"class": {}
					},
					"_example": <<<"
						<p>Here's how you place a horizontally centered <strong>PNG/JPG</strong> image between two paragraphs:</p>

{{#pre}}
<p>Magna et veniam cupidatat do qui qui elit. Eiusmod anim magna nulla sit consectetur veniam dolore id excepteur dolore velit.</p>

{{#figure}}
	{{#item sizing="<mark>auto</mark>"}}
		{{{img src="course/en/images/b-1/pic-1<mark>.png</mark>"}}}
	{{/item}}
{{/figure}}

<p>Magna et veniam cupidatat do qui qui elit. Eiusmod anim magna nulla sit consectetur veniam dolore id excepteur dolore velit.</p>
{{/pre}}

						<p>Here's how you do the same thing again, but this time with an <strong><u>SVG</u></strong> image:</p>


{{#pre}}
<p>Magna et veniam cupidatat do qui qui elit. Eiusmod anim magna nulla sit consectetur veniam dolore id excepteur dolore velit.</p>

{{#figure}}
	{{#item sizing="<mark>6</mark>"}}
		{{{img src="course/en/images/b-1/pic-2<mark>.svg</mark>"}}}
	{{/item}}
{{/figure}}

<p>Magna et veniam cupidatat do qui qui elit. Eiusmod anim magna nulla sit consectetur veniam dolore id excepteur dolore velit.</p>
{{/pre}}

					">>>
				},
				{
					"_desc": "Two graphics between two paragraphs.",
					"_items": [
						{
							"_name": "figure",
							"_content": "PLACE_ITEM_HELPERS_IN_HERE",
							"_properties": {
							}
						},
						{
							"_name": "item",
							"_content": "PLACE_YOUR_CUSTOM_CONTENT_IN_HERE",
							"_properties": {
								"sizing": "4"
							}
						},
						{
							"_name": "item",
							"_content": "PLACE_YOUR_CUSTOM_CONTENT_IN_HERE",
							"_properties": {
								"sizing": "4"
							}
						}
					],
					"properties": {
						"sizing": {
							
						},
						"class": {}
					},
					"_example": <<<"
						<p>Here's how you get two graphics (images/tables/diagrams/etc.) side-by-side between two paragraphs.</p>

{{#pre}}
<p>Enim ad veniam nostrud fugiat nostrud fugiat duis voluptate veniam elit dolor occaecat occaecat.</p>

{{#figure}}
	{{#item sizing="auto"}}
		{{{img src="course/en/images/b-1/pic-1.png"}}}
	{{/item}}
	{{#item sizing="auto"}}
		<table>
			<tr>
				<td>1</td>
				<td>2</td>
				<td>3</td>
			</tr>
			<tr>
				<td>4</td>
				<td>5</td>
				<td>6</td>
			</tr>
			<tr>
				<td>7</td>
				<td>8</td>
				<td>9</td>
			</tr>
		</table>
	{{/item}}
{{/figure}}

<p>Enim ad veniam nostrud fugiat nostrud fugiat duis voluptate veniam elit dolor occaecat occaecat.</p>
{{/pre}}

						<p>The JSONSYNT above will create an image on the left, and a table on the right.</p>

					">>>
				},
				{
					"_desc": "Advanced <code>\\{{#figure}}</code> settings (layout, item order)",
					"_items": [
						{
							"_name": "figure",
							"_content": "PLACE_ITEM_HELPERS_IN_HERE",
							"_properties": {
							}
						}
					],
					"properties": {
						"style": {},
						"direction": {
							"_desc": <<<"
								<p>Has three possible values:</p>

								<ul>
									<li><code>"row"</code> (default)</li>
									<li><code>"column"</code></li>
									<li><code>"wrap"</code></li>
								</ul>
								
								<p>If set to <code>column</code>, will make the figure display all of its <code>&lcub;{#item}&rcub;</code>s in a single, vertical column, regardless of screen size.</p>

								<p>If set to <code>wrap</code>, will display the figure items in a row at all screen sizes (including mobile). The items will wrap as the screen size is reduced.</p>

								<p>Has no effect if there's only one item.</p>
							">>>,
							"type": "String",
							"_isOptional": true
						},
						"reverse": {
							"_desc": <<<"
								<p>If set to <code>true</code>, will make the figure display its <code>&lcub;{#item}&rcub;</code>s in reverse order when on a large viewport.</p>

								<p>Has no effect if there's only one item.</p>
							">>>,
							"type": "Boolean",
							"_isOptional": true
						},
						"align": {
							"_desc": <<<"
								<p>Sets the <a href="https://css-tricks.com/almanac/properties/a/align-items/" target="_blank">vertical alignment</a> of the <code>&lcub;{#item}&rcub;</code>s within the figure.</p>

								<p>Currently supported values are:</p>

								<ul>
									<li><code>&quot;center&quot;</code></li>
									<li><code>&quot;stretch&quot;</code></li>
								</ul>

								<p>Has no effect if there's only one item.</p>
							">>>,
							"type": "String",
							"_isOptional": true
						},
						"wrap": {
							"_desc": <<<"
								<p>Sets to <code>false</code> to prevent the <code>&lcub;{#item}&rcub;</code>s within the figure from automatically wrapping if they're too wide.</p>

								<p>This setting should only be used for customs.</p>

								<p>Has no effect if there's only one item.</p>
							">>>,
							"type": "String",
							"_isOptional": true
						}
					},
					"_example": <<<"
						<p>Feel free to add one or more of the above settings to your <code>\\{{#figure}}</code>. Please keep in mind that the default settings on the figures should be applicable for most scenarios, so only use these settings if you have to.</p>
					">>>
				},
				{
					"_desc": "Advanced <code>\\{{#item}}</code> settings (lightboxes, horizontal scrolling)",
					"_items": [
						{
							"_name": "item",
							"_content": "PLACE_YOUR_CUSTOM_CONTENT_HERE",
							"_properties": {
								"sizing": "6"
							}
						}
					],
					"properties": {
						"lightbox": {},
						"scaleDown": {},
						"scrolling": {
							"_desc": <<<"
								<p>If set to <code>false</code>, will disable horizontal scrolling on the item.</p>

								<p>Only use this if you know what you're doing (like making a custom).</p>
							">>>,
							"type": "Boolean",
							"_isOptional": true
						},
						"class": {},
						"sizing": {
							"_isOptional": false
						},
						"stack": {
							"_desc": <<<"
								<p>(Custom use only)</p>

								<p>If <code>true</code>, will make the <code>{{#item}}</code> stack all of its child elements on top of each other. This can be useful when designing crossfading <a href="#/guide/stickyElement">stick elements</a>.</p>
							">>>,
							"type": "Boolean",
							"_isOptional": true
						},
						"caption": {
							"_desc": <<<"
								<p>(Custom use only)</p>

								<p>Adds text under the item.</p>
							">>>,
							"type": "String",
							"_isOptional": true
						}
					},
					"_example": <<<"
						<p>Feel free to add one or more of the above settings to your <code>\\{{#item}}</code>. Please keep in mind that the default settings on the figures should be applicable for most scenarios, so only use these settings if you have to.</p>
					">>>
				}
			],

        
				"blocks": [],
				"components": [],
				"properties": {
				}
			}
		}

  }
}