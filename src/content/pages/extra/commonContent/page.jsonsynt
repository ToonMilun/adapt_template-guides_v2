{
  "_isEnabled": true,
  "name":    "<span title='A big list of all the different content types and how to make them.'>Developer quicklinks</span>",
  "subName": "",
  "type": "extra",
  "tags": [
    "custom",
    "extra-unrestricted",
    "extra-restricted"
  ],
  "components": {

    "preview": {
      "layouts": [
        {
          "title":      "",
          "hasLabels":  false
        }
      ]
    },

    "description": {
      "body": "Tables/Quoteboxes/Speechbubbles/Diagrams/Images/Looping animated gifs/text/decorative text/etc. Basically, any non-interactive element in a component."
    },

    "modularity": {
      "_parents": {
        "_includeTags": [],
        "_excludeTags": ["extra-unrestricted", "extra-restricted"],
        "_includePages": ["figure", "extraLearning", "resourcePopup", "stickyElement"],
        "_excludePages": []
      }
    },

    "word": {
      "_items": [
        {
          "_header": {
            "_isEnabled": 	true,
            "tn": 			"",
            "notes": 		""
          },
  
          "_comment": 	"",

          "title":		"NOT APPLICABLE",
          "body":			"{{#w-debug}}This page is an index for developer use. No ID doc applicable.{{/w-debug}}",
          "instruction":	"",
          "content":		""
        }
      ]
    },

		"developer": {
		"body": <<<"

			<h2>Grunt tasks</h2>

			<table class="table--commoncontent">
				<tr>
					<td width="190px;" style="vertical-align: top;"><code>grunt help</code></td>
					<td style="display: none;"></td>
					<td>Shows a list of Grunt commands (including a section detailing the ones that you'll be using for Didasko tasks).</td>
				</tr>
				<tr>
					<td style="vertical-align: top;"><code>grunt server</code></td>
					<td style="display: none;"></td>
					<td>Starts a temporary server to host your Adapt build. You will need to have this running in order to preview your work.</td>
				</tr>
				<tr>
					<td style="vertical-align: top;"><code>grunt dev</code></td>
					<td style="display: none;"></td>
					<td>Performs similarly to <code>grunt build</code>, but will automatically re-build whenever you make a change. Have this running while you develop.</td>
				</tr>
				<tr>
					<td style="vertical-align: top;"><code>grunt build</code></td>
					<td style="display: none;"></td>
					<td>(Re)builds your Adapt content. When an Adapt topic is complete, run <code>grunt build</code> to create its SCORM package contents (do <strong>not</strong> run <code>grunt dev</code> for this).</td>
				</tr>
				<tr>
					<td style="vertical-align: top;"><code>grunt build-upload</code></td>
					<td style="display: none;"></td>
					<td>Performs a <code>grunt build</code>, then uploads it to <a href="https://proofingsite.didaskogroup.com/" target="_blank">Didasko's proofing site</a>. The path it will be uploaded to is defined in <code>contentObjects.jsonsynt</code>.</td>
				</tr>
				<tr>
					<td style="vertical-align: top;"><code>grunt clean</code></td>
					<td style="display: none;"></td>
					<td>Empties out the <code>build</code> folder.</td>
				</tr>
			</table>
			

			<h2>Quick links</h2>

			<p>Here's a list of quick links for the different types of extra content you can add inside of templates.</p>
			
			<p>If you suspect there's a content type missing from this list, please contact Milton Plotkin and he'll add it in.</p>
			
			<<<!
			<table class='table--commoncontent'>
				<tr>
					<th colspan="3">Text formatting</th>
				</tr>
				<tr>
					<td colspan="3">The different ways to display text + inline Extras (extras that can sit inside of paragraphs).</th>
				</tr>
				<tr>
					<th>Content type</th>
					<th>How to implement</th>
					<th>Notes</th>
				</tr>
				<tr>
					<td>Paragraphs</td>
					<td>&lt;p&gt;</td>
					<td>
						<p>All paragraphs must be wrapped in these tags.</p>
						<p><strong>Do no use <code>&lt;br&gt;</code> to create paragraph breaks instead!</strong></p>
					</td>
				</tr>
				<tr>
					<td>Breaks ('Enter'/'Return')</td>
					<td>&lt;br/&gt;</td>
					<td>Should <em>only</em> be used in cases where <code>&lt;p&gt;</code> is not suited.</td>
				</tr>
				<tr>
					<td><strong>Bold text</strong></td>
					<td>&lt;strong&gt;</td>
					<td><strong>Do not use <code>&lt;b&gt;</code></strong></td>
				</tr>
				<tr>
					<td><em>Italic text</em></td>
					<td>&lt;em&gt;</td>
					<td>
						<p>Stands for <em>em</em>phasis.</p>
						<p><strong>Do not use <code>&lt;i&gt;</code></strong></p>
					</td>
				</tr>
				<tr>
					<td><u>Underlined text</u></td>
					<td>&lt;u&gt;</td>
					<td></td>
				</tr>
				<tr>
					<td>Hyperlinks</td>
					<td>\\{{#hyperlink}}</td>
					<td>{{{guidelink "hyperlink"}}}</td>
				</tr>
				<tr>
					<td>Download links</td>
					<td>\\{{#download}}</td>
					<td>{{{guidelink "hyperlink"}}}</td>
				</tr>
				<tr>
					<td>Reference popups</td>
					<td>\\{{#reference}}</td>
					<td>{{{guidelink "reference"}}}</td>
				</tr>
				<tr>
					<td>Glossary popups</td>
					<td>\\{{#glossary}}</td>
					<td>{{{guidelink "glossary"}}}</td>
				</tr>


			<tr>
				<td>Tables</td>
				<td>&lt;table&gt;,&lt;tr&gt;,&lt;th&gt;,&lt;td&gt;,etc.</td>
				<td>Normal HTML table formatting applies. Tables can be used inside of {{#pagelink \"figure\"}}{{/pagelink}}.</td>
			</tr>
			<tr>
				<td>Figure containers</td>
				<td>\\{{#figure}},\\{{#item}}</td>
				<td><em>Use these to create decorative layouts in your content.</em><br />{{#pagelink \"figure\"}}{{/pagelink}}</td>
			</tr>
			<tr>
				<td>Images (and complex diagrams)</td>
				<td>\\{{#img}}</td>
				<td>{{#pagelink \"image\"}}{{/pagelink}}</td>
			</tr>
			<tr>
				<td>Animations (Lottie)</td>
				<td>\\{{#anim}}</td>
				<td>{{#pagelink \"animation\"}}{{/pagelink}}</td>
			</tr>
			<tr>
				<td>Code + console boxes</td>
				<td>\\{{#code}}, \\{{#console}}</td>
				<td>{{#pagelink \"code\"}}{{/pagelink}} - Static, {{#pagelink \"codeboxAnim\"}}{{/pagelink}} - Dynamic</td>
			</tr>
			<tr>
				<td>Inline code / console references</td>
				<td>&lt;code&gt;</td>
				<td>Read more about it here: {{#pagelink \"code\"}}{{/pagelink}}</td>
			</tr>
			<tr>
				<td>Marks (highlights)</td>
				<td>&lt;mark&gt;, \\{{#mark}}</td>
				<td>{{#pagelink \"mark\"}}{{/pagelink}}, {{#pagelink \"annotation\"}}{{/pagelink}}</td>
			</tr>
			
			<tr>
				<td>Resource buttons</td>
				<td>\\{{#resource}}</td>
				<td>{{#pagelink \"resource\"}}{{/pagelink}}</td>
			</tr>
			<tr>
				<td>\"Extra\" learning icons</td>
				<td>\\{{#extraLearning}}</td>
				<td>{{#pagelink \"extraLearning\"}}{{/pagelink}}</td>
			</tr>
			<tr>
				<td>Sticky elements</td>
				<td>\\{{#stickyElement}}</td>
				<td>{{#pagelink \"stickyElement\"}}{{/pagelink}}</td>
			</tr>
			<tr>
				<td>*Custom styles for elements*</td>
				<td>Create custom CSS rules and classes for your elements.</td>
				<td>{{#pagelink \"custom\"}}{{/pagelink}}</td>
			</tr>
			<tr>
				<td>*Other*</td>
				<td>Use HTML</td>
				<td>If the content type you want isn't listed here, then please notify an Adapt developer. They will either confirm
				that HTML is appropriate (and add the content type to this list), or will develop a Helper to accomodate your
				request.</td>
			</tr>
			</table>!>>>
		">>>,
      "json": {
        "other": [
			{
				"_title": "Text (static)",
				"_desc": "Paragraphs, titles, italics, inline-code, etc.",
				"_example": <<<"
					<table class='table--commoncontent'>
						<tr>
							<th>Content type</th>
							<th>How to implement</th>
							<th>Notes</th>
						</tr>
						<tr>
							<td>Paragraphs</td>
							<td>&lt;p&gt;</td>
							<td>
								<p>All paragraphs must be wrapped in these tags.</p>
								<p><strong>Do no use <code>&lt;br&gt;</code> to create paragraph breaks instead!</strong></p>
							</td>
						</tr>
						<tr>
							<td>Breaks ('Enter'/'Return')</td>
							<td>&lt;br/&gt;</td>
							<td>Should <em>only</em> be used in cases where <code>&lt;p&gt;</code> is not suited.</td>
						</tr>
						<tr>
							<td><nobr>Non-breaking</nobr> words.</td>
							<td>&lt;nobr&gt;</td>
							<td>
								{{{guidelink "nobr"}}}
							</td>
						</tr>
						<tr>
							<td><nobr>Non-breaking</nobr> spaces.</td>
							<td>
								&#38;nbsp;<br>
								&#38;2nbsp;<br>
								&#38;3nbsp;<br>
								&#38;4nbsp;<br>
								&#38;5nbsp;
							</td>
							<td>
								<p>Use these <strong><u>sparingly</u></strong> when you quickly need to add more than one 'spacebar' character between text.</p>

								<p><strong><u>DO NOT USE</u></strong> for anything else please.</p>
							</td>
						</tr>
						<tr>
							<td><strong>Bold text</strong></td>
							<td>&lt;strong&gt;</td>
							<td><strong>Do not use <code>&lt;b&gt;</code></strong></td>
						</tr>
						<tr>
							<td><em>Italic text</em></td>
							<td>&lt;em&gt;</td>
							<td>
								<p>Stands for <em>em</em>phasis.</p>
								<p><strong>Do not use <code>&lt;i&gt;</code></strong></p>
							</td>
						</tr>
						<tr>
							<td><u>Underlined text</u></td>
							<td>&lt;u&gt;</td>
							<td></td>
						</tr>
						<tr>
							<td><h3>Titles (custom)</h3></td>
							<td>&lt;h3&gt;</td>
							<td>
								<p>Use this to if you need to add a title to a {{#tooltip "graphic"}}graphic{{/tooltip}}, or when you need a quick sub-sub-header over a paragraph of text.</p>

								<p><strong>Only use this to add titles when the template you're using doesn't offer them</strong>.</p>

								<p><em>Guide TBA</em></p>
							</td>
						</tr>
						<tr>
							<td><p><mark>Mark (highlighted) text</mark></p></td>
							<td>
								&lt;mark&gt;<br/>
								\\{{#mark}}<br/>
								\\{{#markBlock}}
							</td>
							<td>{{{guidelink "mark"}}}</td>
						</tr>
						<tr>
							<td><p><span style="opacity: 0.8;">Faded text</span></p></td>
							<td>
								\\{{#fade}}<br/>
								\\{{#fadeBlock}}
							</td>
							<td>{{{guidelink "mark"}}}</td>
						</tr>
						<tr>
							<td><p>Inline <code style="border: 1px solid grey;">code</code></p></td>
							<td>&lt;code&gt;</td>
							<td>{{{guidelink "codebox"}}}</td>
						</tr>
						<tr>
							<td><p><code style="display: block; border: 1px solid grey;">Codeboxes & outputboxes</span></p></td>
							<td>
								\\{{#codebox}}<br/>
								\\{{#outputbox}}
							</td>
							<td>{{{guidelink "codebox"}}}</td>
						</tr>
						<tr>
							<td><span style="display: block; font-style: italic; background: beige; padding: 0.5em; box-shadow: 2px 2px rgb(0 0 0 / 25%);">Blockquotes (quote boxes)</span></td>
							<td>\\{{#blockquote}}</td>
							<td>{{{guidelink "quote"}}}</td>
						</tr>
						<tr>
							<td>Keyboard <kbd style="display: inline-block; background: lightgrey; padding: 0.25em; border-radius: 4px; border-style: groove;">keys</kbd></td>
							<td>&lt;kbd&gt;</td>
							<td>
								{{{guidelink "kbd"}}}
							</td>
						</tr>
						<tr>
							<td><span style="font-family: Times New Roman;">Math (equations)</span></td>
							<td>
								\\{{#m-math}}<br/>
								\\{{#m-a}}<br/>
								\\{{#m-diac}}<br/>
								\\{{#m-frac}}<br/>
								<em>...and more</em>
							</td>
							<td>{{{guidelink "math"}}}</td>
						</tr>
						<tr>
							<td>Inline icons & images<br/>♠♥♦♣</td>
							<td>
								\\{{#inlineIcon}}
							</td>
							<td>{{{guidelink "icon"}}}</td>
						</tr>
					</table>
				">>>
			},
			{
				"_title": "Text (interactive)",
				"_desc": "Hyperlinks, annotations, glossaries, etc.",
				"_example": <<<"
					<table class='table--commoncontent'>
						<tr>
							<th>Content type</th>
							<th>How to implement</th>
							<th>Notes</th>
						</tr>
						<tr>
							<td>Hyperlinks</td>
							<td>\\{{#hyperlink}}</td>
							<td>{{{guidelink "hyperlink"}}}</td>
						</tr>
						<tr>
							<td>Download links</td>
							<td>\\{{#download}}</td>
							<td>{{{guidelink "hyperlink"}}}</td>
						</tr>
						<tr>
							<td>Reference popups</td>
							<td>\\{{#reference}}</td>
							<td>{{{guidelink "reference"}}}</td>
						</tr>
						<tr>
							<td>Glossary popups</td>
							<td>\\{{#glossary}}</td>
							<td>{{{guidelink "glossary"}}}</td>
						</tr>
						<tr>
							<td>Input fields</td>
							<td>\\{{#inputField}}</td>
							<td>{{{guidelink "inputField"}}}</td>
						</tr>
						<tr>
							<td>Output fields</td>
							<td>\\{{#outputField}}</td>
							<td>{{{guidelink "inputField"}}}</td>
						</tr>
						<tr>
							<td>Annotations (click to highlight area)</td>
							<td>\\{{#annotation}}</td>
							<td>{{{guidelink "annotation"}}}</td>
						</tr>
						<tr>
							<td>Popups (click to show popup)</td>
							<td>\\{{#annotation}}</td>
							<td>{{{guidelink "annotation"}}}</td>
						</tr>
					</table>
				">>>
			},
			{
				"_title": "Images",
				"_desc": "Decorative pictures, informative diagrams, block logos, etc.",
				"_example": <<<"
					<table class='table--commoncontent'>
						<tr>
							<th>Content type</th>
							<th>How to implement</th>
							<th>Notes</th>
						</tr>
						<tr>
							<td>Images (decorative)</td>
							<td>\\{{#img}}</td>
							<td>{{{guidelink "image"}}}</td>
						</tr>
						<tr>
							<td>Images (informative)</td>
							<td>\\{{#img}}</td>
							<td>
								<p>If no text: {{{guidelink "image"}}}</p>
								<p>If text: {{{guidelink "imageResp"}}}</p>
							</td>
						</tr>
						<tr>
							<td>Images with highlights</td>
							<td>\\{{#img}} <nobr>\\{{{mark absolute="..."}}}</nobr> \\{{/img}}</td>
							<td>
								<p>{{{guidelink "image"}}}</p>
							</td>
						</tr>
						<tr>
							<td>Images with other images as overlays</td>
							<td>\\{{#img}}...\\{{/img}}</td>
							<td>
								<p>{{{guidelink "image"}}}</p>
							</td>
						</tr>
						<tr>
							<td>Inline icons & images<br/>♠♥♦♣</td>
							<td>
								\\{{#inlineIcon}}
							</td>
							<td>{{{guidelink "icon"}}}</td>
						</tr>
						<tr>
							<td>
								<p>Block logo<br/><br/>(large logo image that sits in the corner of the block)</p>
							</td>
							<td>"_componentLogo": {...}</td>
							<td>
								<p>{{{guidelink "componentLogo"}}}</p>
							</td>
						</tr>
						<tr>
							<td>
								<p>Graph<br/><br/>(bar chart, pie chart, line graph, etc.)</p>
							</td>
							<td>
								\\{{#rgraph}} or 
								\\{{#img}}
							</td>
							<td>
								<p>Depending on your graph, use:<br/>{{{guidelink "rgraph"}}} or {{{guidelink "imageResp"}}}</p>
							</td>
						</tr>
						<tr>
							<td>
								<p>Diagram</p>
							</td>
							<td>
								\\{{#img}}
							</td>
							<td>
								<p>If no text: {{{guidelink "image"}}}</p>
								<p>If text: {{{guidelink "imageResp"}}}</p>
							</td>
						</tr>
					</table>
				">>>
			},
			{
				"_title": "Highlights (Mark)",
				"_desc": "Everything to do with highlighting text/graphics",
				"_example": <<<"
					<table class='table--commoncontent'>
						<tr>
							<th>Content type</th>
							<th>How to implement</th>
							<th>Notes</th>
						</tr>
						<tr>
							<td>Marked text</td>
							<td>
								&lt;mark&gt;<br/>
								\\{{#mark}}<br/>
								\\{{#markBlock}}
							</td>
							<td>{{{guidelink "mark"}}}</td>
						</tr>
						<tr>
							<td>Marked table cells</td>
							<td>
								class="mark"
							</td>
							<td>{{{guidelink "mark"}}}</td>
						</tr>
						<tr>
							<td>Images with marks</td>
							<td>\\{{#img}} <nobr>\\{{{mark absolute="..."}}}</nobr> \\{{/img}}</td>
							<td>
								<p>{{{guidelink "image"}}}</p>
							</td>
						</tr>
						<tr>
							<td>Annotations (click to show highlights)</td>
							<td>\\{{#annotation}}</td>
							<td>{{{guidelink "annotation"}}}</td>
						</tr>
						<tr>
							<td>Alternative mark colours</td>
							<td>class="theme-*"</td>
							<td>{{{guidelink "mark"}}}</td>
						</tr>
						<tr>
							<td><p><span style="opacity: 0.8;">Faded text</span></p></td>
							<td>
								\\{{#fade}}<br/>
								\\{{#fadeBlock}}
							</td>
							<td>{{{guidelink "mark"}}}</td>
						</tr>
					</table>
				">>>
			},
			{
				"_title": "Tables",
				"_desc": "Tables",
				"_example": <<<"

					<p>Tables are made using <a href="https://www.w3schools.com/html/html_tables.asp">normal HTML</a>.</p> 

					<table class='table--commoncontent'>
						<tr>
							<th>Content type</th>
							<th>How to implement</th>
							<th>Notes</th>
						</tr>
						<tr>
							<td>Table</td>
							<td>
								&lt;table&gt;<br/>
								&lt;tr&gt;<br/>
								&lt;th&gt;<br/>
								&lt;td&gt;
							</td>
							<td>
								<p>All tables must be in figures: {{{guidelink "figure"}}}.</p>
								<p>If your table is too big on phones, enable the figure <code>lightbox</code>.</p>
							</td>
						</tr>
						<tr>
							<td>Big tables</td>
							<td>
								&lt;table&gt;<br/>
								&lt;tr&gt;<br/>
								&lt;th&gt;<br/>
								&lt;td&gt;
							</td>
							<td>
								<p>Wrap the table in a figure: {{{guidelink "figure"}}}.</p>
								<p>Enable the <code>lightbox</code> feature.</p>
							</td>
						</tr>
						<tr>
							<td>Marked table cells</td>
							<td>
								&lt;td class="mark"&gt;
							</td>
							<td>{{{guidelink "mark"}}}</td>
						</tr>
						<tr>
							<td>Invisible table cells</td>
							<td>
								&lt;td data-invis&gt;
							</td>
							<td>Add the <code>data-invis</code> attribute to the table cell.</td>
						</tr>
						<tr>
							<td>Tables with input/output fields</td>
							<td>
								&lt;td&gt;<br/>&nbsp;&nbsp;\\{{#inputField}}<br/>&lt;/td&gt;<br/>
								&lt;td&gt;<br/>&nbsp;&nbsp;\\{{#outputField}}<br/>&lt;/td&gt;
							</td>
							<td>{{{guidelink "inputField"}}}</td>
						</tr>
						<tr>
							<td>Table themes</td>
							<td>
								theme-math<br/>
								theme-frequency<br/>
								theme-ghost<br/>
								theme-base<br/>
								theme-small
							</td>
							<td>
								<p>Apply one or more of the classes on the left to the root <code>&lt;table&gt;</code> element to change its appearance.</p>

								<p>Guide TBA.</p>
							</td>
						</tr>
						<tr>
							<td>Tables with custom overlays</td>
							<td>
								N/A
							</td>
							<td>
								<p>Custom. Make manually using custom HTML and CSS.</p>
							</td>
						</tr>
					</table>
				">>>
			},
			{
				"_title": "Stylizing / themes",
				"_desc": "Misc. ways to change the visual appearance of certain elements.",
				"_example": <<<"

					<table class='table--commoncontent'>
						<tr>
							<th>Content type</th>
							<th>How to implement</th>
							<th>Notes</th>
						</tr>
						<tr>
							<td>Table appearance</td>
							<td>
								theme-math<br/>
								theme-frequency<br/>
								theme-ghost<br/>
								theme-base<br/>
								theme-small
							</td>
							<td>
								<p>Apply one or more of the classes on the left to the root <code>&lt;table&gt;</code> element to change its appearance.</p>

								<p>Guide TBA.</p>
							</td>
						</tr>
						<tr>
							<td>Mark colors</td>
							<td>
								theme-1<br/>
								theme-2<br/>
								theme-3<br/>
								theme-4
							</td>
							<td>
								<p>For custom use only (use the default colour of mark unless specified).</p>
								<p>Use one of these classes to change the colour of the mark.</p>
								<p>{{{guidelink "mark"}}}</p>
							</td>
						</tr>
					</table>
				">>>
			}
		],
        "blocks": [],
        "components": [],
        "properties": {
          "_resourcePopup": {
            "_header": "Partials",
            "_desc": "ResourcePopup Helper popup settings set here."
          },
          "_resourcePopup._items": {
            "_header": "",
            "_desc": "Define each ResourcePopup's popup in here."
          },
          "_resourcePopup._items._id": {
            "_desc": "The resourcePopup helper's value <strong>must</strong> match this value."
          }
        }
      }
    }

  }
}