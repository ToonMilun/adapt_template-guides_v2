{
  "_isHidden": false,
  "name":    "Animation",
  "subName": "decorative",
  "type": "extra",
  "tags": [
    "media",
    "audio",
    "image-small",
    "image-large",
    "extra-restricted"
  ],
  "components": {

    "preview": {
      "layouts": [
        {
          "title":      "",
          "hasLabels":  false
        }
      ]
    },

    "description": {
      "body": "A <strong>basic</strong> animation that can be placed inside content for <strong>decoration</strong>. Will play once when the animation is scrolled to / revealed. For more complex animations, use the media screen."
    },

    "modularity": {
      "body": "    {{#tba}}Subject to change.{{/tba}}    ",
      "_parents": {
        "_includeTags": ["all"],
        "_excludeTags": ["extra-unrestricted", "extra-restricted"],
        "_includePages": ["figure", "extraLearning", "resourcePopup", "stickyElement"],
        "_excludePages": ["hotgraphic-mini", "media"]
      }
    },

    "word": {
      "_items": [
        {
          "_header": {
            "_isEnabled": 	true,
            "tn": 			"{{#w-debug}}Insert template compatible with this extra here{{/w-debug}}",
            "notes": 		""
          },
  
          "_comment": 	"This is an example of just one possible way to use this Extra. Most Extras are flexible, so feel free to come up with your own ways to use them.",
          
          "title":		"Title",
          "body":			"{{#w-yellow}}Animation][Create a simple animation of a tree falling over using {{#id-img}}{{/id-img}} and place it to the side.{{/w-yellow}}<br/><br/>{{#id-standup-text basic='true'}}{{/id-standup-text}}",
          "instruction":	"{{#id-instruction}}{{/id-instruction}}",
          "content":		""
        }
      ]
    },

    "developer": {
      "body": "        <p>Most of the development time spent on a Lottie animation takes place outside of Adapt. As such, please ensure you read the following instructions before beginning:</p>        <ol>          <li>            Before you animate anything, evaluate whether what you're dealing with is a Lottie animation at all:            <ul>              <li>If the animation has a diagram performing simple modifications on itself, chances are, it's a {{#guidelink \"stickyElement\"}}{{/guidelink}} instead.</li>              <li>If the animation has a very long duration, and contains audio with dialogue, it's probably better as a {{#guidelink \"media\"}}{{/guidelink}} video instead.</li>              <li>If the animation contains a large quantity of text, it may need to be re-designed into its own template altogether.</li>              <li>If the animation is short, mostly decorative, and contains mostly images, then it's a decorative animation (a.k.a, proceed to the next step.</li>            </ul>          </li>          <li>You will need to create the animation using Adobe After Effects, and export it using the Lottie plugin (<a href=\"content/pages/extra/animation/Bodymovin for Lottie.docx\">see the guide for installation + use here</a>). <strong><u>This likely requires admin permission</u></strong>.</li>          <li>Try to use SVGs wherever possible (makes it easier for us to scale the animation in post).</li>.          <li>Set your canvas size to around 600px by 600px. Depending on the context the animation sits in, you can tweak these dimensions if needed (if you want the animation to take up the full-width for example, give it 860px width).</li>          <li>Do <strong>not</strong> give the animation a background colour (we want the background transparent).</li>          <li>When making your animation, please make sure you adhere to the following Lottie rules (if you don't, the Lottie may not render/may have bugs):            <ul>              <li>Do not use 3D.</li>              <li>Do not apply multiple masks to one object (Firefox doesn't display it properly). <strong>Note:</strong> in After Effects, the \"trim paths\" tool creates a mask.</li>            </ul>          </li>          <li>After your animation is done, if you can, please trim any negative space that wasn't used.</li>          <li>Export your animation to inside the following folder (replacing the X's as necessary): <code>src/course/en/images/b-<em>X</em>/<strong>animX</strong>/...</code></li>        </ol><br/><h2>IMPORTANT!</h2><p>You <strong>must</strong> ensure that all images for the Lottie are placed inside: <code>src/course/en/images/b-X/animX/<u>images</u>/</code>. Ensure that the folder is called <code><u>i</u>mages</code>, and NOT <code><u>I</u>mages</code>! All folders in Adapt should be lowercase!</p>      ",
      "json": {
        "helpers": [
          {
            "_desc": "Autoplaying decorative animation.",
            "_items": [
              {
                "_name": "lottie",
                "_content": "",
                "_properties": {
                  "src": "course/en/images/b-X/animX/animX.json",
                  "autoplay": true,
				  "loops": 1
                }
              }
            ],
            "properties": {
				"name": {
					"_isOptional": true,
					"_desc": "Provides a way for certain other helpers to target this helper."
				},
              "src": {
                "_desc": "Location of the *.json file that you exported from After Effects using Lottie."
              },
              "autoplay": {
                "_desc": "Leave as true for now. Will be expanded upon later.",
				"type": "Boolean"
              },
              "loops": {
			  	"_isOptional": true,
                "_desc": "<p>How many times an <em>autoplay</em>ing Lottie should loop itself before stopping.</p><p>You can use decimals (for example, if <code>loops</code> is <code>1.5</code>, the animation will pause midway through its second loop).</p><p>This is can be useful if you need to trim off some frames from the end of older animations.</p>",
				"type": "Number"
              },
              "controls": {
			  	      "_isOptional": true,
                "_desc": "                  <p>What type of controls this animation should have. If not set, then will default to <code>&quot;none&quot;</code>.</p>                  <p>Possible values are:</p>                  <ul>                    <li><code>&quot;none&quot;</code>: don't show any controls on the Lottie. Use this if your animation is <em>purely decorative</em>.</li>                    <li><code>&quot;basic&quot;</code>: show a replay icon in the top-right corner once the animation completes. Use this if your animation is <em>short</em> and <em>informative</em>.</li>                    <li><code>&quot;full&quot;</code>: show proper video controls (with a timeline). Use if your animation is <em>long</em> and <em>informative</em>.</li>                  </ul>                ",
				        "type": "String"
              }
            },
            "_example": "<p>Lottie animations should be placed inside of the {{{guidelink \"figure\"}}} helper. You can tweak their size by changing their <code>&lcub;&lcub;#item&rcub;&rcub;</code> helper's <code>sizing</code> property.</p><p>Here's an example:</p><code class='example'>&lcub;&lcub;#figure&rcub;&rcub;<br class=\"br\" />    &lcub;&lcub;#item sizing=\"12\"&rcub;&rcub;<br class=\"br\" /><mark data-is-block=\"true\"><br class=\"br\" />        &lcub;&lcub;&lcub;lottie src=\"course/en/images/b-3/anim1/anim1.json\" autoplay=true loops=1&rcub;&rcub;&rcub;<br class=\"br\" /></mark><br class=\"br\" />    &lcub;&lcub;/item&rcub;&rcub;<br class=\"br\" />&lcub;&lcub;/figure&rcub;&rcub;</code>             "
          }
        ],
        "blocks": [],
        "components": [],
        "properties": {
        }
      }
    }
  }
}