{
  "name": "Accordion",
  "subName": "",
  "tags": [
    "button",
    "text-large",
    "text-medium",
    "image-small",
    "image-large",
    "items",
	"non-sequential"
  ],
  "components": {
	"preview": {
		"_examples": [
			{
				"title": "IT08-9-4",
				"_url": "accordion-1"
			},
			{
				"title": "SET-RW-14",
				"_url": "accordion-2"
			}
		]
	},
    "description": {
		"body": <<<"
			
			<p>The Accordion template displays one or more reveals hidden behind large buttons. It is good for displaying learning material where the reveals are non-sequential.</p>

			<p>Each reveal usually contains a {{#tooltip "graphic"}}graphic{{/tooltip}}, but it can be removed so the reveal only displays text.</p>

			{{#note}}All Accordion reveals can be open at the same time (example: opening R2 won't close R1).{{/note}}

			<h3>ToggleTabs vs. Accordion</h3>

			<p>The {{{guidelink "toggleTabs"}}} and Accordion templates both display information in a similar way.</p>
			
			<p>The Toggle Tabs template takes up less vertical room, and is one of the few templates that supports the {{{guidelink "stickyElement"}}} extra. However, <strong>Toggle Tabs must have similarly sized reveals</strong> for layout reasons. For example: it cannot have R2 being thrice the height of R1 (however, mild fluctuations in height are acceptable and expected).</p>

			<p>The Accordion's advantage is that each of its reveals can have a different size, and it also allows the user to see all reveals open at once. However, the Accordion is more suited for displaying non-sequential reveals.</p>
		">>>,
		"_examples": [
			<<<"
				Covering multiple unordered, but related types of content that requires a large amount of text and/or additional customs.<br/>Example: Apples, Bananas, Coconuts.
			">>>
		]
    },
    "modularity": {
      "body": <<<"
	  	<p>An Accordion's reveals can contain most nestable templates.</p>
	  ">>>,
      "_parents": {
        "_includeTags": [],
        "_excludeTags": [],
        "_includePages": [
			"static-multi",
			"static-single"
		],
        "_excludePages": []
      }
    },
    "word": {
      "_items": [
        {
          "_header": {
            "_isEnabled": 	true,
            "tn": 			"",
            "notes": 		""
          },
  
          "_comment": 	"",
          "title":		"Title",
          "body":			"{{#id-standup-text}}{{/id-standup-text}}",
          "instruction":	"",
          "content":		"",
  
          "_items": [
            {
              "_comment": "<strong>Note:</strong> Graphics in reveals are <u>optional</u>.",
              "_header":	"{{#id-item-header}}1{{/id-item-header}}",
              "title": 	"ButtonTitle",
              "body": 	"{{#id-reveal-text}}{{/id-reveal-text}}",
              "instruction": 	"",
              "content": ""
            },
            {
              "_comment": "",
              "_header":	"{{#id-item-header}}2{{/id-item-header}}",
              "title": 	"ButtonTitle",
              "body": 	"{{#id-reveal-text}}{{/id-reveal-text}}",
              "instruction": 	"",
              "content": ""
            }
          ]
        }
      ]
    },
    "developer": {
      "body": "<p>If you want to have text on the left + non-image figure on the right in the accordion reveals, you'll need to use the {{#pagelink \"figure\"}}{{/pagelink}} helper.</p>",
      "json": {
        "blocks": [],
        "components": [
          {
            "_desc": "",
            "_items": [
              {
                "_id": "",
                "_parentId": "b-X",
                "_component": "accordion",
                "_layout": "full",
                "_classes": "",
                "title": "XXX",
                "body": "<<<XXX>>>",
                "_itemSizing": {
                  "_width": 6,
                  "_maxWidth": 4,
                  "_float": false
                },
                "_items": [
                  {
                    "title": "XXX",
                    "body": "<<<XXX>>>",
                    "_graphic": {
                      "src": "course/en/images/b-X/pic-X.jpg",
                      "style": "circle",
                      "alt": ""
                    },
                    "_classes": ""
                  }
                ],
				"footer": "<<<>>>",
                "<<<~": "",
                "_setCompletionOn": "allItems",
                "_shouldCollapseItems": false,
                "_type": "component",
                "~>>>": ""
              }
            ]
          }
        ],
        "properties": {
          "_items._graphic": {
            "_isOptional": true
          },
          "_shouldCollapseItems": {
            "_desc": "Whether clicking one item should collapse all others."
          },
          "_itemSizing": {
          	"_header": "Item settings"
          },
				  "_itemSizing._width": {
          	"_desc": "The maximum width the graphic will take"
          },
				  "_items._graphic": {
            "_isOptional": true
          }
        },
        "_defunct": {
          "comment": "Removed \"instruction\" (our current standard doesn't show instruction text for accordions).",
          "instruction": ""
        }
      }
    }
  }
}