define([
	"core/js/adapt"
  ], function(Adapt) {

	/**
	 * Define helpers specific to this project here.
	 * Be SURE to prefix them all with "c-".
	 */

	var helpers = {

        "c-stringtable": function() {

            let context = arguments[arguments.length-1];
			let content = context.fn(this); // Get the contents of the helper (they will be split by character).

			let _chars = content.split(""); // Split the whole string to an array.

			let start = parseInt(context.hash.start) || 0;
			let markedIdxs = (context.hash.mark || "").split(",");
			let fadedIdxs = (context.hash.fade || "").split(",");
			let chars = [];

			// Allow for a string to be used in place of the indexes row.
			let head = context.hash.head;

			let _gridWidth = 0;

			_.each(_chars, (char, idx) => {

				let _idx = head ? head[idx] : ("" + (idx + start));
				chars.push({
					idx: _idx,
					char: char,
					isMarked: _.includes(markedIdxs, head ? ("" + idx) : ("" + (start + idx))), // Some items need to be highlighted.
					isFaded: _.includes(fadedIdxs, head ? ("" + idx) : ("" + (start + idx)))    // And some items need to be faded.
				})

				
				// Determine the longest amount of characters in the indexes.
				if (("" + _idx).length > _gridWidth) _gridWidth = ("" + _idx).length;
			});
			_gridWidth = _gridWidth * 0.9;

            let html = Handlebars.templates["c-stringtable"](_.extend(
				context.hash,
				{
					content: content,
					chars: chars,
					start: start,
					_gridWidth: _gridWidth
				}
			));
            return new Handlebars.SafeString(html);
		},

		"c-arraytable": function() {

            let context = arguments[arguments.length-1];
			let content = context.fn(this); // Get the contents of the helper (they will be split by character).

			let _chars = content.split(","); // Split the whole string to an array of elements.
			let _gridWidth = 0;
			_.each(_chars, e => {
				_.each(e.split(" "), f => {
					// Determine the longest amount of text among the elements.
					if (f.length > _gridWidth) _gridWidth = f.length;
				});
			});
			_gridWidth = Math.round(_gridWidth * 0.7);

			let start = parseInt(context.hash.start) || 0;
			let markedIdxs = (context.hash.mark || "").split(",");
			let fadedIdxs = (context.hash.fade || "").split(",");
			let chars = [];

			// Allow for a string to be used in place of the indexes row.
			let head = context.hash.head;

			_.each(_chars, (char, idx) => {
				chars.push({
					idx: head ? head[idx] : ("" + (idx + start)),
					char: char,
					isMarked: _.includes(markedIdxs, head ? ("" + idx) : ("" + (start + idx))), // Some items need to be highlighted.
					isFaded: _.includes(fadedIdxs, head ? ("" + idx) : ("" + (start + idx)))    // And some items need to be faded.
				})
			});

            let html = Handlebars.templates["c-stringtable"](_.extend(
				context.hash,
				{
					content: content,
					chars: chars,
					start: start,
					_gridWidth: _gridWidth
				}
			));
            return new Handlebars.SafeString(html);
		}
	}

	for (var name in helpers) {
        if (helpers.hasOwnProperty(name)) {
            Handlebars.registerHelper(name, helpers[name]);
        }
    }
});